﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMusic : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] audioClips;

    int currentClipIndex;
    bool newSong = true;
    Coroutine currentSong;

    void Update()
    {
        if(newSong)
        {
            StopCoroutine(PlaySong());
            newSong = false;
            currentSong = StartCoroutine(PlaySong());
            currentClipIndex = (currentClipIndex + 1) % audioClips.Length;
        }
    }

    IEnumerator PlaySong()
    {
        audioSource.clip = audioClips[currentClipIndex];
        audioSource.Play();
        while(audioSource.isPlaying)
        {
            yield return null;
        }
        newSong = true;
    }
}
