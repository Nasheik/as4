//Maya ASCII 2018ff09 scene
//Name: badGuy_Model01.ma
//Last modified: Sun, Nov 25, 2018 12:45:59 AM
//Codeset: 1252
requires maya "2018ff09";
requires "mtoa" "3.0.1.1";
requires "mtoa" "3.0.1.1";
currentUnit -l foot -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201807191615-2c29512b8a";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "353A4BBC-47B0-4FAA-83BD-E4A5DD88815F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 18.702012692119176 6.2939281018413205 18.509904440071104 ;
	setAttr ".r" -type "double3" -378.93835272824134 45.399999999946964 1.1324292716143913e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "703E7D1E-4F5D-1A81-1112-78B0E6CAA58E";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 0.0032808398950131233;
	setAttr ".fcp" 328.08398950131232;
	setAttr ".fd" 0.16404199475065617;
	setAttr ".coi" 27.333139691236138;
	setAttr ".ow" 0.32808398950131235;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 45.441992557568916 -39.943703015153247 48.64948935397554 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "1AF7C502-48C5-1203-1D5A-3CA79A57727A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.069528788012778633 32.811679790026247 0.0082367013369947402 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "803F84BC-4703-1519-428D-A58D32282143";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.0032808398950131233;
	setAttr ".fcp" 328.08398950131232;
	setAttr ".fd" 0.16404199475065617;
	setAttr ".coi" 32.811679790026247;
	setAttr ".ow" 2.910364068236476;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "5BC27260-4FE2-DA43-39F8-E5BD84CBB294";
	setAttr ".t" -type "double3" -0.098129043633598451 1.1599141717754746 32.811679790026247 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "05E73336-403E-FA51-DDE2-4491647EA535";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ncp" 0.0032808398950131233;
	setAttr ".fcp" 328.08398950131232;
	setAttr ".fd" 0.16404199475065617;
	setAttr ".coi" 32.811679790026247;
	setAttr ".ow" 1.8183508997143703;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "D15CCA6B-4223-E953-D98C-28877DA96CE5";
	setAttr ".t" -type "double3" 33.412042587602102 1.6130587026749152 1.3071306401483138 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "F700876C-4D1B-A357-C36A-F296044A958F";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".ncp" 0.0032808398950131233;
	setAttr ".fcp" 328.08398950131232;
	setAttr ".fd" 0.16404199475065617;
	setAttr ".coi" 33.412042587602102;
	setAttr ".ow" 2.7979339995892691;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 42.676064693197461 38.484665482258421 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "imagePlane1";
	rename -uid "DE43D52C-4937-AEAF-0522-CFB32DDE1C36";
	setAttr ".t" -type "double3" 0 0.2168992792334114 -32.051526126282823 ;
	setAttr ".s" -type "double3" 9.9156025600926547 9.9156025600926547 9.9156025600926547 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "B61A88B1-4FC1-FD23-C05A-838A9295C4D4";
	setAttr -k off ".v";
	setAttr ".fc" 102;
	setAttr ".imn" -type "string" "D:/documents/school/animation/TBS//refImgs/BadGuyRef_Frnt.jpg";
	setAttr ".cov" -type "short2" 1912 3024 ;
	setAttr ".dic" yes;
	setAttr ".dlc" no;
	setAttr ".w" 0.62729658792650922;
	setAttr ".h" 0.99212598425196852;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "imagePlane2";
	rename -uid "3ADFDDD9-4E60-A86B-038D-358FDFFB4F19";
	setAttr ".t" -type "double3" -24.631487372085399 0 1.1638522209611326 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".s" -type "double3" 9.9156025600926547 9.9156025600926547 9.9156025600926547 ;
createNode imagePlane -n "imagePlaneShape2" -p "imagePlane2";
	rename -uid "3B123EAD-41E1-8340-B2D2-3C9211181777";
	setAttr -k off ".v";
	setAttr ".fc" 102;
	setAttr ".imn" -type "string" "D:/documents/school/animation/TBS//refImgs/BadGuyRef_Side.jpg";
	setAttr ".cov" -type "short2" 2200 3024 ;
	setAttr ".dic" yes;
	setAttr ".dlc" no;
	setAttr ".w" 0.72178477690288712;
	setAttr ".h" 0.99212598425196841;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "skull01";
	rename -uid "3FDA514D-414A-C631-0C9E-7FA9FB61FE04";
	setAttr ".t" -type "double3" 0 1.4001333560760321 1.262620258604279 ;
createNode mesh -n "skull0Shape1" -p "skull01";
	rename -uid "7AB61061-46C5-9264-6399-97B55DDB0FDF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000001490116119 0.29474133253097534 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 47 ".pt";
	setAttr ".pt[0]" -type "float3" 0.051619265 0 0 ;
	setAttr ".pt[2]" -type "float3" -0.051619265 0 0 ;
	setAttr ".pt[3]" -type "float3" 0.0042004967 -0.14318612 0 ;
	setAttr ".pt[4]" -type "float3" 0 -0.077397905 0.027089262 ;
	setAttr ".pt[5]" -type "float3" -0.0042004967 -0.14318612 0 ;
	setAttr ".pt[6]" -type "float3" -0.050807945 0 0 ;
	setAttr ".pt[7]" -type "float3" 0 -0.059931844 0.049943201 ;
	setAttr ".pt[8]" -type "float3" 0.050807945 0 0 ;
	setAttr ".pt[12]" -type "float3" 0 -0.30465341 0.014982961 ;
	setAttr ".pt[13]" -type "float3" 0 -0.30465341 0.014982961 ;
	setAttr ".pt[14]" -type "float3" 0 -0.30465341 0.014982961 ;
	setAttr ".pt[15]" -type "float3" -0.072056957 -0.18478976 -0.0099886376 ;
	setAttr ".pt[16]" -type "float3" 3.6591694e-09 -0.18478976 -0.0099886376 ;
	setAttr ".pt[17]" -type "float3" 0.072056957 -0.18478976 -0.0099886376 ;
	setAttr ".pt[19]" -type "float3" 0 -0.030959157 0.058048416 ;
	setAttr ".pt[21]" -type "float3" 0 -0.30465341 0.014982961 ;
	setAttr ".pt[22]" -type "float3" 0 -0.30465341 0.014982961 ;
	setAttr ".pt[23]" -type "float3" 0 -0.30465341 0.014982961 ;
	setAttr ".pt[24]" -type "float3" 0.0023859644 -0.14159846 -0.04031999 ;
	setAttr ".pt[25]" -type "float3" 0 -0.050571006 0.017728431 ;
	setAttr ".pt[26]" -type "float3" -0.0023859644 -0.14159846 -0.04031999 ;
	setAttr ".pt[27]" -type "float3" 0.012789813 0.015087047 0.007450575 ;
	setAttr ".pt[28]" -type "float3" -1.6085412e-09 -0.13484666 -0.029965919 ;
	setAttr ".pt[29]" -type "float3" -0.012789813 0.015087047 0.007450575 ;
	setAttr ".pt[30]" -type "float3" -0.1090474 -0.034960244 -0.074914813 ;
	setAttr ".pt[31]" -type "float3" 0 -0.034960244 -0.074914813 ;
	setAttr ".pt[32]" -type "float3" 0.1090474 -0.034960244 -0.074914813 ;
	setAttr ".pt[33]" -type "float3" -0.020348353 -0.16855611 -0.026757212 ;
	setAttr ".pt[34]" -type "float3" 0 -0.093917429 0.017728435 ;
	setAttr ".pt[35]" -type "float3" 0.020348353 -0.16855611 -0.026757212 ;
	setAttr ".pt[36]" -type "float3" -0.0092510488 -0.14318612 0 ;
	setAttr ".pt[37]" -type "float3" 0 -0.077397905 0.027089262 ;
	setAttr ".pt[38]" -type "float3" 0.0092510488 -0.14318612 0 ;
	setAttr ".pt[39]" -type "float3" 0.062529325 0.081723645 0 ;
	setAttr ".pt[40]" -type "float3" 0 0.081723645 0 ;
	setAttr ".pt[41]" -type "float3" -0.062529325 0.081723645 0 ;
	setAttr ".pt[42]" -type "float3" 0.022539774 0.068745345 -0.022915114 ;
	setAttr ".pt[43]" -type "float3" 0 0.068745345 -0.022915114 ;
	setAttr ".pt[44]" -type "float3" -0.022539774 0.068745345 -0.022915114 ;
	setAttr ".pt[45]" -type "float3" 0.025514167 0.027089262 -0.050308626 ;
	setAttr ".pt[46]" -type "float3" 0.00016402766 -0.04256884 -0.081267789 ;
	setAttr ".pt[47]" -type "float3" 0.10904742 -0.04256884 -0.081267789 ;
	setAttr ".pt[48]" -type "float3" 0.045236673 0 0 ;
	setAttr ".pt[54]" -type "float3" -0.045236673 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.10904742 -0.04256884 -0.081267789 ;
	setAttr ".pt[56]" -type "float3" -0.00016402766 -0.04256884 -0.081267789 ;
	setAttr ".pt[57]" -type "float3" -0.025514167 0.027089262 -0.050308626 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "body01";
	rename -uid "F6DC2ACE-4F03-1C04-D6B3-148C4E152DFB";
	setAttr ".t" -type "double3" 0 -4.9275605289506235 0 ;
	setAttr ".s" -type "double3" 0.97881992492062253 1 1 ;
createNode mesh -n "body0Shape1" -p "body01";
	rename -uid "47542CDD-49B0-08F5-D8E8-2D8EFC0BB27A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "l_arm_grp01";
	rename -uid "CA6CD8D7-46BD-7090-1252-5DAF8240E26A";
createNode transform -n "l_arm01" -p "l_arm_grp01";
	rename -uid "A67B8E24-41A2-22FB-F55C-679C931AAECE";
	setAttr ".t" -type "double3" 1.740879017162992 -1.1155973065139797 1.3531376237658514 ;
	setAttr ".s" -type "double3" 1 0.38888890183580305 2.4685185343087404 ;
createNode mesh -n "l_arm0Shape1" -p "l_arm01";
	rename -uid "DF185B99-410C-99CE-7F68-10A74F0BDCCC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "l_arm01";
	rename -uid "A7282D7E-47E9-02E2-AD5F-D9AE039BF9E7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.5 0 0.625
		 0 0.375 0.25 0.5 0.25 0.625 0.25 0.375 0.5 0.5 0.5 0.625 0.5 0.375 0.75 0.5 0.75
		 0.625 0.75 0.375 1 0.5 1 0.625 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5
		 0 0.5 0.5 0.5 0.5 0.5 -0.5 0.5 -0.5 0 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0 -0.5 -0.5
		 0.5 -0.5 -0.5;
	setAttr -s 20 ".ed[0:19]"  0 1 0 1 2 0 3 4 0 4 5 0 6 7 0 7 8 0 9 10 0
		 10 11 0 0 3 0 1 4 1 2 5 0 3 6 0 4 7 1 5 8 0 6 9 0 7 10 1 8 11 0 9 0 0 10 1 1 11 2 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 9 -3 -9
		mu 0 4 0 1 4 3
		f 4 1 10 -4 -10
		mu 0 4 1 2 5 4
		f 4 2 12 -5 -12
		mu 0 4 3 4 7 6
		f 4 3 13 -6 -13
		mu 0 4 4 5 8 7
		f 4 4 15 -7 -15
		mu 0 4 6 7 10 9
		f 4 5 16 -8 -16
		mu 0 4 7 8 11 10
		f 4 6 18 -1 -18
		mu 0 4 9 10 13 12
		f 4 7 19 -2 -19
		mu 0 4 10 11 14 13
		f 4 -20 -17 -14 -11
		mu 0 4 2 15 16 5
		f 4 17 8 11 14
		mu 0 4 17 0 3 18;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "r_arm_grp01";
	rename -uid "A7B4DEC4-45A3-EA5F-01E5-DA82897D34EA";
	setAttr ".s" -type "double3" -1 1 1 ;
createNode transform -n "r_arm01" -p "r_arm_grp01";
	rename -uid "701692E6-4D6F-98B7-3AFA-7DBA1C907202";
	setAttr ".t" -type "double3" 1.740879017162992 -1.1155973065139797 1.3531376237658514 ;
	setAttr ".s" -type "double3" 1 0.38888890183580305 2.4685185343087404 ;
createNode mesh -n "r_arm0Shape1" -p "r_arm01";
	rename -uid "BC00C4C3-49C3-E9B8-A20F-F78DEB2E31AF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 148 ".uvst[0].uvsp[0:147]" -type "float2" 0.375 0 0.5 0 0.625
		 0 0.375 0.25 0.5 0.25 0.625 0.25 0.375 0.5 0.5 0.5 0.625 0.5 0.375 0.75 0.5 0.75
		 0.625 0.75 0.375 1 0.5 1 0.625 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25 0.2439805
		 0.25 0.375 0.3810195 0.5 0.3810195 0.625 0.3810195 0.75601953 0.25 0.625 0.86898053
		 0.75601953 0 0.5 0.86898053 0.2439805 0 0.375 0.86898053 0.29403323 0.25 0.375 0.33096677
		 0.5 0.33096677 0.625 0.33096677 0.70596683 0.25 0.625 0.91903323 0.70596683 0 0.5
		 0.91903323 0.29403323 0 0.375 0.91903323 0.33331525 0.25 0.375 0.29168475 0.5 0.29168475
		 0.625 0.29168475 0.66668481 0.25 0.625 0.95831519 0.66668475 0 0.5 0.95831519 0.33331522
		 0 0.375 0.95831519 0.4375 0.86898053 0.28073987 0 0.375 0.9057399 0.46189278 0.8965503
		 0.5 0.86149681 0.625 0.86095577 0.76404428 0 0.625 0.3898201 0.7648201 0.25 0.5 0.38993424
		 0.23458819 0.25 0.375 0.39041182 0.2366544 0 0.375 0.8616544 0.43255767 0.8657462
		 0.24031745 0 0.375 0.86531746 0.23928434 0.25 0.375 0.38571566 0.27291879 0.25 0.375
		 0.35208121 0.44870698 0.3810195 0.44982761 0.38539869 0.44251269 0.34802258 0.4375
		 0.86898053 0.46189278 0.8965503 0.375 0.9057399 0.28073987 0 0.27291879 0.25 0.2439805
		 0.25 0.375 0.86531746 0.43255767 0.8657462 0.23928434 0.25 0.24031745 0 0.2439805
		 0 0.28073987 0 0.27291879 0.25 0.2439805 0.25 0.23928434 0.25 0.24031745 0 0.2439805
		 0 0.28073987 0 0.27291879 0.25 0.2439805 0.25 0.23928434 0.25 0.24031746 0 0.5 0
		 0.5 1 0.5 0.25 0.5 0.95831519 0.5 0.29168475 0.56303072 0 0.56303072 1 0.56303072
		 0.95831519 0.56303072 0.91903323 0.56303072 0.86898053 0.56303072 0.861224 0.56303072
		 0.75 0.56303072 0.5 0.56303072 0.38987669 0.56303072 0.3810195 0.56303072 0.33096677
		 0.56303072 0.29168475 0.56303072 0.25 0.43577278 0 0.43577278 1 0.43577275 0.95831519
		 0.43577275 0.91903323 0.41724569 0.90127206 0.41724569 0.90127206 0.40538639 0.86898053
		 0.40298349 0.86552596 0.40298349 0.86552596 0.43577278 0.86157781 0.43577278 0.75
		 0.43577278 0.5 0.43577275 0.39017963 0.41137984 0.38556159 0.41083503 0.38101953
		 0.40782344 0.35010797 0.43577278 0.33096677 0.43577278 0.29168475 0.43577278 0.25
		 0.625 0.92312479 0.70187533 0 0.625 0.32687527 0.70187533 0.25 0.56303072 0.32687527
		 0.5 0.32687527 0.5 0.92312479 0.56303072 0.92312473 0.29799309 0 0.375 0.92299312
		 0.43577275 0.92299312 0.5 0.92299312 0.5 0.32700691 0.43577278 0.32700691 0.29799309
		 0.25 0.375 0.32700691;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 112 ".vt[0:111]"  -0.49519429 -2.18929648 0.23832469 0.036006212 -2.17813921 0.23685607
		 0.50000006 -2.17813921 0.23685607 -0.50000006 -2.25725579 0.39390141 0.036006212 -2.25725579 0.39390141
		 0.50000006 -2.25725579 0.39390141 -0.03455092 0.088268705 -1.38537192 0 0.088268705 -1.38537192
		 0.03455092 0.088268705 -1.38537192 -0.03455092 -0.088269673 -1.38537192 0 -0.088269673 -1.38537192
		 0.03455092 -0.088269673 -1.38537192 -0.51637524 0.53157383 0.039110575 0 0.6363278 0.01318178
		 0.50000006 0.41904661 -0.013278555 0.50000006 -0.21346037 -0.013937343 0.1544881 -0.50000042 -0.024078118
		 -0.49965572 0.33007297 0.32309684 0 0.32442147 0.32070366 0.50000006 0.32442147 0.32070366
		 0.50000006 -0.25027493 0.22236075 0.019057717 -0.50230026 0.18448204 -0.41817421 -0.17810687 0.26645738
		 -0.50000006 -1.78278553 0.58794487 0.036006212 -1.78278553 0.58794487 0.50000006 -1.78278553 0.58794487
		 0.50000006 -1.46510088 0.4645302 0.036006212 -1.46029937 0.46347392 -0.50000006 -1.46029937 0.46347392
		 -0.10155272 -0.64680928 0.020749427 -0.44876936 -0.18356147 0.19058207 -0.1310422 -0.64807558 0.15205298
		 0.1544881 -0.47677875 -0.10970215 0.4686074 -0.5162816 -0.11589193 0.46557242 0.23665537 -0.12465505
		 0 0.45338866 -0.12597194 -0.46325755 0.23299624 -0.1315385 -0.45081323 -0.51902735 -0.1155166
		 -0.1382222 -0.63609308 -0.067222022 -0.44068763 -0.51979285 -0.07748384 -0.48065606 0.33872813 -0.073635086
		 -0.51760834 0.58968055 0.25459054 -0.20517202 0.6363278 0.01318178 -0.19376485 0.55338758 -0.068149641
		 -0.19191717 0.54576856 0.22724922 -0.39841208 -0.76538026 0.034285326 -0.79958093 -0.6042856 0.10545213
		 -0.31279242 -0.69413519 0.12327984 -0.59549087 -0.37003589 0.1864963 -0.90872508 0.42432788 0.13482045
		 -0.7383433 0.59428668 0.22974722 -0.39457291 -0.7076211 -0.036434744 -1.0030366182 -0.75085145 0.063367739
		 -1.13003385 0.1092317 0.04930139 -0.69913304 -1.54438317 0.1355069 -0.69400769 -1.95123613 0.19826104
		 -0.60819006 -1.38556671 0.15563671 -0.59193701 -1.79181814 0.20456527 -0.78293443 -1.72798193 0.12147551
		 -0.82816356 -2.095077753 0.17955293 -0.86250961 -0.85644835 0.15568073 -0.74175227 -0.59241587 0.18124565
		 -0.89157397 -0.57064146 0.27399957 -1.037998199 -0.73268694 0.2542986 -1.20530427 -0.95584399 0.21464868
		 -0.99031001 -1.079621196 0.13967544 -0.017489458 -2.17813921 0.23685607 -0.017489458 -2.25725579 0.39390141
		 -0.017489458 -1.46029937 0.46347392 -0.017489458 -1.78278553 0.58794487 0.26997325 -2.38487697 0.17370139
		 0.26997325 -1.36457157 0.44484463 0.26157099 -0.50230026 0.18448204 0.32871109 -0.50458604 -0.024078118
		 0.3128815 -0.49669793 -0.11282331 0.017422162 -0.088269673 -1.38537192 0.017422162 0.088268705 -1.38537192
		 0.23476303 0.34410179 -0.1253079 0.25212303 0.52676463 -0.00016073797 0.25212303 0.68735951 0.37282011
		 0.26997325 -1.91060543 0.62424922 0.26997325 -2.529001 0.39390141 -0.26541209 -2.38546991 0.17570478
		 -0.26541209 -1.36457157 0.44484463 -0.22192648 -0.49641082 0.19765022 -0.26768145 -0.50994408 0.17568345
		 -0.42325911 -0.51367855 0.14343861 -0.59107345 -0.69144261 0.10433184 -0.66849923 -0.73810887 0.010370469
		 -0.30518699 -0.56767714 -0.068332963 -0.16707385 -0.49730003 -0.10877535 -0.017752886 -0.088269688 -1.38537192
		 -0.017752886 0.08826869 -1.38537192 -0.23803009 0.34014705 -0.12883215 -0.34167448 0.43671721 -0.070327081
		 -0.35904935 0.56690949 0.025084995 -0.34561652 0.70616013 0.24037072 -0.25690904 0.68735939 0.37282011
		 -0.26541209 -1.91060543 0.62424922 -0.26541209 -2.52697015 0.39390141 0.50000006 -0.37680754 0.46741846
		 0.50000006 0.10494117 0.56837273 0.25398228 0.41676331 0.6188423 0.017174844 0.10494117 0.56837273
		 0.038126439 -0.38939387 0.46603197 0.26244614 -0.59211183 0.43143469 -0.46818572 -0.38650435 0.48448363
		 -0.23308794 -0.58922237 0.44089112 -0.0044642803 -0.38650435 0.46598178 -0.010684483 0.11200237 0.56829959
		 -0.25776619 0.42546898 0.61882204 -0.50000006 0.11200237 0.56829959;
	setAttr -s 220 ".ed";
	setAttr ".ed[0:165]"  0 82 1 1 70 1 3 99 1 4 81 1 6 92 1 7 76 1 9 91 1 10 75 1
		 0 3 1 1 4 1 2 5 1 3 23 1 4 24 1 5 25 1 6 9 1 7 10 1 8 11 1 9 37 1 10 32 1 11 33 1
		 12 40 1 13 35 1 14 34 1 15 20 1 16 21 1 12 95 1 13 78 1 14 15 1 15 73 1 16 29 1 17 41 1
		 18 13 1 19 14 1 20 100 1 21 104 1 22 106 1 17 97 1 18 79 1 19 20 1 20 72 1 21 84 1
		 22 17 1 23 111 1 24 103 1 25 101 1 26 2 1 27 1 1 28 0 1 23 98 1 24 80 1 25 26 1 26 71 1
		 68 83 1 28 23 1 30 22 1 29 31 1 31 85 1 31 21 1 32 16 1 33 15 1 34 8 1 35 7 1 36 6 1
		 37 39 1 32 74 1 33 34 1 34 77 1 35 93 1 36 37 1 37 90 1 29 38 1 38 32 1 38 89 1 40 36 1
		 39 40 1 41 12 1 30 41 1 42 13 1 40 94 1 43 42 1 42 44 1 44 96 1 44 18 1 43 35 1 29 45 1
		 45 87 1 31 47 1 45 47 1 30 48 1 47 86 1 46 48 1 12 49 1 41 50 1 48 50 1 50 49 1 38 51 1
		 39 52 1 51 88 1 45 51 1 52 46 1 40 53 1 52 53 1 49 53 1 46 60 1 49 63 1 54 55 1 48 61 1
		 54 56 1 50 62 1 56 57 1 57 55 1 52 65 1 53 64 1 58 59 1 58 54 1 55 59 1 60 54 1 61 56 1
		 62 57 1 63 55 1 64 59 1 65 58 1 60 61 1 61 62 1 62 63 1 63 64 1 64 65 1 65 60 1 66 67 1
		 68 66 1 21 108 1 67 69 1 69 109 1 21 18 1 69 68 1 27 24 1 70 2 1 71 27 1 72 21 1
		 73 16 1 74 33 1 75 11 1 76 8 1 77 35 1 78 14 1 79 19 1 80 25 1 81 5 1 70 71 1 71 105 1
		 72 73 1 73 74 1 74 75 1 75 76 1 76 77 1 77 78 1 78 79 1 79 102 1 80 81 1 81 70 1
		 82 66 1 83 28 1 84 22 1 85 30 1 86 48 1 87 46 1;
	setAttr ".ed[166:219]" 88 52 1 89 39 1 90 32 1 91 10 1 92 7 1 93 36 1 94 43 1
		 95 42 1 96 41 1 97 18 1 98 69 1 99 67 1 82 83 1 83 107 1 84 85 1 85 86 1 86 87 1
		 87 88 1 88 89 1 89 90 1 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 110 1
		 98 99 1 99 82 1 100 26 1 101 19 1 102 80 1 103 18 1 104 27 1 105 72 1 100 101 1 101 102 1
		 102 103 1 103 104 1 104 105 1 105 100 1 106 28 1 107 84 1 108 68 1 109 18 1 110 98 1
		 111 17 1 106 107 1 107 108 1 108 109 1 109 110 1 110 111 1 111 106 1;
	setAttr -s 110 -ch 440 ".fc[0:109]" -type "polyFaces" 
		f 4 195 160 128 -178
		mu 0 4 131 113 95 97
		f 4 159 136 10 -148
		mu 0 4 112 100 2 5
		f 4 189 172 83 67
		mu 0 4 125 126 71 58
		f 4 155 144 22 66
		mu 0 4 108 109 22 56
		f 4 187 170 15 -170
		mu 0 4 123 124 7 10
		f 4 153 142 16 -142
		mu 0 4 106 107 8 11
		f 4 58 29 70 71
		mu 0 4 53 26 49 63
		f 4 151 140 59 28
		mu 0 4 104 105 54 24
		f 4 27 -60 65 -23
		mu 0 4 23 25 55 57
		f 4 68 63 74 73
		mu 0 4 59 61 64 66
		f 4 82 31 -78 80
		mu 0 4 72 31 21 70
		f 4 156 145 32 -145
		mu 0 4 109 110 32 22
		f 4 38 -24 -28 -33
		mu 0 4 33 35 25 23
		f 4 150 -29 23 39
		mu 0 4 103 104 24 34
		f 4 182 -86 87 89
		mu 0 4 118 119 73 74
		f 4 -106 107 109 110
		mu 0 4 86 83 84 85
		f 4 217 212 176 132
		mu 0 4 144 145 130 99
		f 4 203 198 146 44
		mu 0 4 134 136 111 42
		f 4 50 -197 202 -45
		mu 0 4 43 45 133 135
		f 4 149 207 196 51
		mu 0 4 102 139 132 44
		f 4 179 215 210 52
		mu 0 4 115 142 143 98
		f 4 219 208 53 42
		mu 0 4 146 140 47 39
		f 4 194 177 131 -177
		mu 0 4 130 131 97 99
		f 4 158 147 13 -147
		mu 0 4 111 112 5 42
		f 4 -46 -51 -14 -11
		mu 0 4 2 45 43 5
		f 4 148 -52 45 -137
		mu 0 4 101 102 44 14
		f 4 178 -53 129 -161
		mu 0 4 114 115 98 96
		f 4 -54 47 8 11
		mu 0 4 39 47 0 3
		f 4 180 -57 57 40
		mu 0 4 116 117 52 36
		f 4 -58 -56 -30 24
		mu 0 4 36 52 49 26
		f 4 152 141 19 -141
		mu 0 4 105 106 11 54
		f 4 -66 -20 -17 -61
		mu 0 4 57 55 15 16
		f 4 154 -67 60 -143
		mu 0 4 107 108 56 8
		f 4 188 -68 61 -171
		mu 0 4 124 125 58 7
		f 4 17 -69 62 14
		mu 0 4 17 61 59 18
		f 4 186 169 18 -169
		mu 0 4 122 123 10 53
		f 4 -72 72 185 168
		mu 0 4 53 63 121 122
		f 4 183 -98 -99 85
		mu 0 4 119 120 80 73
		f 4 -114 114 105 115
		mu 0 4 87 88 83 86
		f 4 -77 54 41 30
		mu 0 4 68 50 37 29
		f 4 190 173 -80 -173
		mu 0 4 126 127 70 71
		f 4 191 -82 -81 -174
		mu 0 4 127 128 72 70
		f 4 192 175 -83 81
		mu 0 4 128 129 31 72
		f 4 -84 79 77 21
		mu 0 4 58 71 70 21
		f 4 55 86 -88 -85
		mu 0 4 49 52 74 73
		f 4 56 181 -90 -87
		mu 0 4 52 117 118 74
		f 4 76 92 -94 -89
		mu 0 4 50 68 77 76
		f 4 75 91 -95 -93
		mu 0 4 68 19 78 77
		f 4 184 -73 95 97
		mu 0 4 120 121 63 80
		f 4 -71 84 98 -96
		mu 0 4 63 49 73 80
		f 4 -75 96 101 -101
		mu 0 4 66 64 82 81
		f 4 20 100 -103 -92
		mu 0 4 19 66 81 78
		f 4 122 117 -108 -117
		mu 0 4 89 90 84 83
		f 4 123 118 -110 -118
		mu 0 4 90 91 85 84
		f 4 124 119 -111 -119
		mu 0 4 91 92 86 85
		f 4 126 121 113 -121
		mu 0 4 93 94 88 87
		f 4 127 116 -115 -122
		mu 0 4 94 89 83 88
		f 4 125 120 -116 -120
		mu 0 4 92 93 87 86
		f 4 90 106 -123 -104
		mu 0 4 27 76 90 89
		f 4 93 108 -124 -107
		mu 0 4 76 77 91 90
		f 4 94 104 -125 -109
		mu 0 4 77 78 92 91
		f 4 102 112 -126 -105
		mu 0 4 78 81 93 92
		f 4 -102 111 -127 -113
		mu 0 4 81 82 94 93
		f 4 99 103 -128 -112
		mu 0 4 82 27 89 94
		f 4 216 -133 134 -211
		mu 0 4 143 144 99 98
		f 4 135 43 205 200
		mu 0 4 46 41 137 138
		f 4 -135 -132 -129 -130
		mu 0 4 98 99 97 96
		f 4 12 -136 46 9
		mu 0 4 4 41 46 1
		f 4 -138 -149 -2 -47
		mu 0 4 46 102 101 13
		f 4 206 -150 137 -201
		mu 0 4 138 139 102 46
		f 4 -140 -151 138 -25
		mu 0 4 26 104 103 36
		f 4 64 -152 139 -59
		mu 0 4 53 105 104 26
		f 4 7 -153 -65 -19
		mu 0 4 10 106 105 53
		f 4 5 -154 -8 -16
		mu 0 4 7 107 106 10
		f 4 -144 -155 -6 -62
		mu 0 4 58 108 107 7
		f 4 26 -156 143 -22
		mu 0 4 21 109 108 58
		f 4 37 -157 -27 -32
		mu 0 4 31 110 109 21
		f 4 49 -199 204 -44
		mu 0 4 41 111 136 137
		f 4 3 -159 -50 -13
		mu 0 4 4 112 111 41
		f 4 1 -160 -4 -10
		mu 0 4 1 100 112 4
		f 4 -162 -179 -1 -48
		mu 0 4 48 115 114 12
		f 4 214 -180 161 -209
		mu 0 4 141 142 115 48
		f 4 -164 -181 162 -55
		mu 0 4 51 117 116 38
		f 4 -182 163 88 -165
		mu 0 4 118 117 51 75
		f 4 -166 -183 164 -91
		mu 0 4 28 119 118 75
		f 4 -167 -184 165 -100
		mu 0 4 79 120 119 28
		f 4 -168 -185 166 -97
		mu 0 4 65 121 120 79
		f 4 -186 167 -64 69
		mu 0 4 122 121 65 62
		f 4 6 -187 -70 -18
		mu 0 4 9 123 122 62
		f 4 4 -188 -7 -15
		mu 0 4 6 124 123 9
		f 4 -172 -189 -5 -63
		mu 0 4 60 125 124 6
		f 4 78 -190 171 -74
		mu 0 4 67 126 125 60
		f 4 25 -191 -79 -21
		mu 0 4 20 127 126 67
		f 4 -175 -192 -26 -76
		mu 0 4 69 128 127 20
		f 4 36 -193 174 -31
		mu 0 4 30 129 128 69
		f 4 48 -213 218 -43
		mu 0 4 40 130 145 147
		f 4 2 -195 -49 -12
		mu 0 4 3 131 130 40
		f 4 0 -196 -3 -9
		mu 0 4 0 113 131 3
		f 4 -203 -34 -39 -198
		mu 0 4 135 133 35 33
		f 4 157 -204 197 -146
		mu 0 4 110 136 134 32
		f 4 -205 -158 -38 -200
		mu 0 4 137 136 110 31
		f 4 -206 199 -134 34
		mu 0 4 138 137 31 36
		f 4 -139 -202 -207 -35
		mu 0 4 36 103 139 138
		f 4 -208 201 -40 33
		mu 0 4 132 139 103 34
		f 4 -163 -210 -215 -36
		mu 0 4 38 116 142 141
		f 4 -216 209 -41 130
		mu 0 4 143 142 116 36
		f 4 133 -212 -217 -131
		mu 0 4 36 31 144 143
		f 4 193 -218 211 -176
		mu 0 4 129 145 144 31
		f 4 -219 -194 -37 -214
		mu 0 4 147 145 129 30
		f 4 -42 35 -220 213
		mu 0 4 29 37 140 146;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "r_arm01";
	rename -uid "BC0453C3-4A69-22CC-F71C-EA961FA086FD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 19 ".uvst[0].uvsp[0:18]" -type "float2" 0.375 0 0.5 0 0.625
		 0 0.375 0.25 0.5 0.25 0.625 0.25 0.375 0.5 0.5 0.5 0.625 0.5 0.375 0.75 0.5 0.75
		 0.625 0.75 0.375 1 0.5 1 0.625 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 12 ".vt[0:11]"  -0.5 -0.5 0.5 0 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5
		 0 0.5 0.5 0.5 0.5 0.5 -0.5 0.5 -0.5 0 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0 -0.5 -0.5
		 0.5 -0.5 -0.5;
	setAttr -s 20 ".ed[0:19]"  0 1 0 1 2 0 3 4 0 4 5 0 6 7 0 7 8 0 9 10 0
		 10 11 0 0 3 0 1 4 1 2 5 0 3 6 0 4 7 1 5 8 0 6 9 0 7 10 1 8 11 0 9 0 0 10 1 1 11 2 0;
	setAttr -s 10 -ch 40 ".fc[0:9]" -type "polyFaces" 
		f 4 0 9 -3 -9
		mu 0 4 0 1 4 3
		f 4 1 10 -4 -10
		mu 0 4 1 2 5 4
		f 4 2 12 -5 -12
		mu 0 4 3 4 7 6
		f 4 3 13 -6 -13
		mu 0 4 4 5 8 7
		f 4 4 15 -7 -15
		mu 0 4 6 7 10 9
		f 4 5 16 -8 -16
		mu 0 4 7 8 11 10
		f 4 6 18 -1 -18
		mu 0 4 9 10 13 12
		f 4 7 19 -2 -19
		mu 0 4 10 11 14 13
		f 4 -20 -17 -14 -11
		mu 0 4 2 15 16 5
		f 4 17 8 11 14
		mu 0 4 17 0 3 18;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A46969C4-480A-8FDF-375C-BDA9B0BF6DC9";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "8CD40425-44A4-AFFA-5C3A-D8915553B074";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "0A330059-40FC-A0E5-8284-3687F21D8334";
createNode displayLayerManager -n "layerManager";
	rename -uid "AA20FA79-4D56-7555-DF63-C585BB55BC51";
	setAttr ".cdl" 1;
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "EE15E225-4871-A9CA-ACD5-2B83855F03B7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "40F4929C-4462-8BD2-D519-8981B0F07875";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "FB68073C-4022-70F3-B506-859A19DC1CDB";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "09DC95F5-47E4-AFBD-F3B8-B192BF8B1805";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 1\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1074\n            -height 680\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n"
		+ "                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n"
		+ "                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"straight\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"straight\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1074\\n    -height 680\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1074\\n    -height 680\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "D3C69F16-445A-A1C1-4DB0-399A99604E1A";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode displayLayer -n "refImgs_lyr01";
	rename -uid "3EA57095-496D-3418-1F8B-6F985F2493F6";
	setAttr ".dt" 2;
	setAttr ".do" 1;
createNode polyCube -n "polyCube1";
	rename -uid "805037E7-4B04-0F86-A51C-95AE30AC4268";
	setAttr ".ax" -type "double3" 0 1 0 ;
	setAttr ".w" 1;
	setAttr ".h" 1;
	setAttr ".d" 1;
	setAttr ".sw" 2;
	setAttr ".sd" 2;
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit1";
	rename -uid "661B1320-4D28-3035-D68E-88AC8F9252E4";
	setAttr -s 7 ".e[0:6]"  0.52407801 0.52407801 0.52407801 0.47592199
		 0.47592199 0.47592199 0.52407801;
	setAttr -s 7 ".d[0:6]"  -2147483637 -2147483636 -2147483635 -2147483629 -2147483630 -2147483631 
		-2147483637;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "DA93EDF0-416C-55C6-49BA-05831019B700";
	setAttr -s 7 ".e[0:6]"  0.617975 0.617975 0.617975 0.382025 0.382025
		 0.382025 0.617975;
	setAttr -s 7 ".d[0:6]"  -2147483637 -2147483636 -2147483635 -2147483625 -2147483624 -2147483623 
		-2147483637;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "F0F7FB73-4D84-9D20-454A-BF952DE3C52E";
	setAttr -s 7 ".e[0:6]"  0.51483798 0.51483798 0.51483798 0.48516199
		 0.48516199 0.48516199 0.51483798;
	setAttr -s 7 ".d[0:6]"  -2147483637 -2147483636 -2147483635 -2147483613 -2147483612 -2147483611 
		-2147483637;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "2DF6FB8B-4CEE-746E-51B7-389334CAA96E";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk";
	setAttr ".tk[0]" -type "float3" 0 -51.149685 3.2449117 ;
	setAttr ".tk[1]" -type "float3" 0 -51.149685 3.2449117 ;
	setAttr ".tk[2]" -type "float3" 0 -51.149685 3.2449117 ;
	setAttr ".tk[3]" -type "float3" 0 -84.041153 8.0316534 ;
	setAttr ".tk[4]" -type "float3" 0 -84.041153 8.0316534 ;
	setAttr ".tk[5]" -type "float3" 0 -84.041153 8.0316534 ;
	setAttr ".tk[6]" -type "float3" 14.18689 -12.549554 -26.986137 ;
	setAttr ".tk[7]" -type "float3" -8.2941941e-15 -12.549554 -26.986137 ;
	setAttr ".tk[8]" -type "float3" -14.18689 -12.549554 -26.986137 ;
	setAttr ".tk[9]" -type "float3" 14.18689 12.549554 -26.986137 ;
	setAttr ".tk[10]" -type "float3" -8.2941941e-15 12.549554 -26.986137 ;
	setAttr ".tk[11]" -type "float3" -14.18689 12.549554 -26.986137 ;
	setAttr ".tk[18]" -type "float3" 0 -5.3516154 5.5176458 ;
	setAttr ".tk[19]" -type "float3" 0 -5.3516154 5.5176458 ;
	setAttr ".tk[20]" -type "float3" 0 -5.3516154 5.5176458 ;
	setAttr ".tk[21]" -type "float3" 0 -0.0700984 1.3656099 ;
	setAttr ".tk[22]" -type "float3" 0 -0.0700984 1.3656099 ;
	setAttr ".tk[23]" -type "float3" 0 -0.0700984 1.3656099 ;
	setAttr ".tk[24]" -type "float3" 0 -69.579277 19.028309 ;
	setAttr ".tk[25]" -type "float3" 0 -69.579277 19.028309 ;
	setAttr ".tk[26]" -type "float3" 0 -69.579277 19.028309 ;
	setAttr ".tk[27]" -type "float3" 0 -26.352116 14.666605 ;
	setAttr ".tk[28]" -type "float3" 0 -26.352116 14.666605 ;
	setAttr ".tk[29]" -type "float3" 0 -26.352116 14.666605 ;
createNode polySplit -n "polySplit4";
	rename -uid "749AC674-4FA0-0FF3-8E4C-5AA3B1BBE3B0";
	setAttr ".v[0]" -type "float3"  -0.15242884 -0.50126678 0.11087969;
	setAttr -s 3 ".e[0:2]"  0.5 14 0.73441303;
	setAttr -s 3 ".d[0:2]"  -2147483618 0 -2147483623;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit5";
	rename -uid "9489CC8D-4854-244D-B849-04AD605A2975";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483590 -2147483624;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit6";
	rename -uid "5552305E-4273-3272-E992-EEA5DE002147";
	setAttr ".v[0]" -type "float3"  -0.22460125 -0.47387385 -0.11045811;
	setAttr -s 3 ".e[0:2]"  0 6 0.93710101;
	setAttr -s 3 ".d[0:2]"  -2147483592 0 -2147483630;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit7";
	rename -uid "1226DADE-4CBB-BBC0-3795-00BB0CDEE1BE";
	setAttr -s 7 ".e[0:6]"  0 0.93255401 0.0739666 0.074926101 0.0789399
		 0.93842602 1;
	setAttr -s 7 ".d[0:6]"  -2147483587 -2147483629 -2147483626 -2147483627 -2147483628 -2147483631 
		-2147483586;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "4C3BF67D-414C-751D-16AE-7CB25F137EE1";
	setAttr ".ics" -type "componentList" 1 "e[62]";
	setAttr ".cv" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "F7C52917-44B8-5771-19F4-3A855F6442A0";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[16]" -type "float3" 4.7087965 0 0 ;
	setAttr ".tk[22]" -type "float3" 4.7087965 0 0 ;
	setAttr ".tk[30]" -type "float3" 2.4678187 0 0 ;
	setAttr ".tk[32]" -type "float3" 2.4678187 0 0 ;
	setAttr ".tk[33]" -type "float3" 4.7087965 0 0 ;
createNode polySplit -n "polySplit8";
	rename -uid "11F9AC0B-4FD6-86EB-CF99-F085B1129350";
	setAttr ".v[0]" -type "float3"  -0.18913664 -0.48880768 -0.061082907;
	setAttr -s 3 ".e[0:2]"  0 6 1;
	setAttr -s 3 ".d[0:2]"  -2147483592 0 -2147483576;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit9";
	rename -uid "B92A55BF-4274-227F-7E7F-D1B601A07258";
	setAttr -s 2 ".e[0:1]"  1 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483575 -2147483582;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit10";
	rename -uid "4B6207BC-4163-753F-B6FB-BEBCE5DB198F";
	setAttr -s 2 ".e[0:1]"  1 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483582 -2147483628;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit11";
	rename -uid "2EAEAA07-432E-B795-1FD0-DB87CFFF2D5A";
	setAttr -s 2 ".e[0:1]"  1 0.42184401;
	setAttr -s 2 ".d[0:1]"  -2147483623 -2147483616;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit12";
	rename -uid "7E76EE6F-4A69-F8DB-72D6-D0936B98AC43";
	setAttr -s 2 ".v[0:1]" -type "float3"  -0.1937649 0.48484594 -0.07418149 
		-0.22994934 0.38425142 0.22724928;
	setAttr -s 5 ".e[0:4]"  1 2 0.589656 10 1;
	setAttr -s 5 ".d[0:4]"  -2147483628 0 -2147483622 1 -2147483616;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit13";
	rename -uid "3190D525-46BA-E3DB-69A3-D19C5D0EA847";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483564 -2147483610;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit14";
	rename -uid "959D828A-44F9-3D0C-EB8B-A28075032728";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483566 -2147483627;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "653A3CEE-4038-E231-1975-3C982C86614F";
	setAttr ".ics" -type "componentList" 2 "f[14:15]" "f[37:38]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.38888890183580305 0 0 0 0 2.4685185343087404 0
		 53.061992443127998 -34.003405902546099 41.243634772383153 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.4551471 -1.1159258 1.4994315 ;
	setAttr ".rs" 49577;
	setAttr ".lt" -type "double3" 0 -5.8179825240068599e-16 0.074370866320201498 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.2408790246722385 -1.3106985945900582 1.1610665807071141 ;
	setAttr ".cbx" -type "double3" 1.6694153634283293 -0.92115285851634077 1.8377963952222269 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "BBE0C78D-470B-C173-8928-2082668F3EF4";
	setAttr ".ics" -type "componentList" 2 "f[15]" "f[38]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.38888890183580305 0 0 0 0 2.4685185343087404 0
		 53.061992443127998 -34.003405902546099 41.243634772383153 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.77197963 -1.1414932 1.6225362 ;
	setAttr ".rs" 43745;
	setAttr ".lt" -type "double3" 8.7419135797256423e-16 -1.4569855966209403e-17 0.67675235348783158 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.55153488136793949 -1.3618333671594505 1.4515849166536097 ;
	setAttr ".cbx" -type "double3" 0.99242446266276263 -0.92115298019395153 1.7934875461952733 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "93E3855A-494D-00F9-EC96-9784F03FA535";
	setAttr ".uopa" yes;
	setAttr -s 27 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[1]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[2]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[3]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[4]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[5]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[22]" -type "float3" 1.0784106 1.7763568e-14 0.62715876 ;
	setAttr ".tk[23]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[24]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[25]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[26]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[27]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[28]" -type "float3" 0 0 11.247967 ;
	setAttr ".tk[29]" -type "float3" 1.7573874 0 1.3663443 ;
	setAttr ".tk[30]" -type "float3" -1.320928 0 0.47892702 ;
	setAttr ".tk[31]" -type "float3" 0.47748271 1.9539925e-14 1.254964 ;
	setAttr ".tk[38]" -type "float3" 1.5518734 0 -0.18711664 ;
	setAttr ".tk[41]" -type "float3" 1.0116808 0 0.85257411 ;
	setAttr ".tk[45]" -type "float3" -6.0517035 1.7763568e-14 1.6175556 ;
	setAttr ".tk[46]" -type "float3" -12.362129 5.3290705e-14 3.6114221 ;
	setAttr ".tk[47]" -type "float3" -7.2268114 1.7763568e-14 -0.11439335 ;
	setAttr ".tk[48]" -type "float3" -6.4394817 -1.7763568e-14 -0.54710078 ;
	setAttr ".tk[49]" -type "float3" -11.744428 6.5725203e-14 4.4529629 ;
	setAttr ".tk[50]" -type "float3" -6.4394817 -1.9539925e-14 -0.54710078 ;
	setAttr ".tk[51]" -type "float3" -8.2305698 5.3290705e-14 0.99595612 ;
	setAttr ".tk[52]" -type "float3" -19.325783 3.5527137e-14 3.7131639 ;
	setAttr ".tk[53]" -type "float3" -19.325783 3.907985e-14 3.7131639 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "4545B5E6-4737-490C-20A1-8B867C71D403";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[54:59]" -type "float3"  4.39471436 10.3942194 5.71893454
		 4.60222054 -10.53271008 3.48385715 1.26125062 11.81095982 4.35875511 2.4204607 -5.3225441
		 2.13245702 7.38581324 7.44694996 6.79327536 7.45663404 -13.89724731 5.61087179;
createNode polySplit -n "polySplit15";
	rename -uid "385BB118-47CB-25F4-7111-35BF3C716F11";
	setAttr -s 7 ".e[0:6]"  0.60677499 0.60677499 0.60677499 0.60677499
		 0.60677499 0.60677499 0.60677499;
	setAttr -s 7 ".d[0:6]"  -2147483545 -2147483542 -2147483540 -2147483544 -2147483536 -2147483537 
		-2147483545;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "C6075173-4C06-5DE9-4E9A-37A5D09C3364";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[9]" "e[12]" "e[34]" "e[43]" "e[46]";
createNode polyTweak -n "polyTweak5";
	rename -uid "7BC4D4EC-448B-717D-1402-079825A83A17";
	setAttr ".uopa" yes;
	setAttr -s 47 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[1]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[2]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[3]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[4]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[5]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[12]" -type "float3" 0 -2.4674411 0.32917067 ;
	setAttr ".tk[13]" -type "float3" 0 4.1552882 0.32917067 ;
	setAttr ".tk[14]" -type "float3" 0 -2.4674411 0.32917067 ;
	setAttr ".tk[15]" -type "float3" 0 -0.27718514 0 ;
	setAttr ".tk[23]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[24]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[25]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[26]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[27]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[28]" -type "float3" 0 0 -28.881969 ;
	setAttr ".tk[30]" -type "float3" 0 2.3197334 0 ;
	setAttr ".tk[32]" -type "float3" 0 -0.081570327 -3.7252903e-09 ;
	setAttr ".tk[33]" -type "float3" 0 -1.3426702 -8.9406967e-08 ;
	setAttr ".tk[34]" -type "float3" 0 -7.0984812 0.0034546852 ;
	setAttr ".tk[35]" -type "float3" 0 -0.48041201 0.0031266212 ;
	setAttr ".tk[36]" -type "float3" 0 -7.147603 -2.3841858e-07 ;
	setAttr ".tk[37]" -type "float3" 0 -1.2822585 5.9604645e-08 ;
	setAttr ".tk[38]" -type "float3" 0 -0.014503777 -4.6566129e-10 ;
	setAttr ".tk[39]" -type "float3" 0 -0.75835657 4.4703484e-08 ;
	setAttr ".tk[40]" -type "float3" 0 -4.7983632 0.16522956 ;
	setAttr ".tk[42]" -type "float3" 0 4.1552882 0.32917067 ;
	setAttr ".tk[43]" -type "float3" 0 2.0891547 0.18385291 ;
	setAttr ".tk[46]" -type "float3" 5.3808475 8.1131325 -0.4615002 ;
	setAttr ".tk[48]" -type "float3" 3.8263853 8.8807325 -0.79739612 ;
	setAttr ".tk[49]" -type "float3" 5.3852754 -3.3786097 -0.66476226 ;
	setAttr ".tk[50]" -type "float3" 4.1012712 0.72548777 -1.0967423 ;
	setAttr ".tk[51]" -type "float3" 0 -0.026657999 -9.3132257e-10 ;
	setAttr ".tk[52]" -type "float3" 6.8431573 6.4809074 -0.17022759 ;
	setAttr ".tk[53]" -type "float3" 7.085567 -6.2372942 -0.0728724 ;
	setAttr ".tk[54]" -type "float3" 12.583233 -16.838408 -12.348934 ;
	setAttr ".tk[55]" -type "float3" 11.605723 -39.013355 -9.5954428 ;
	setAttr ".tk[56]" -type "float3" 11.410935 -13.817627 -11.894464 ;
	setAttr ".tk[57]" -type "float3" 10.830356 -35.747974 -9.8447819 ;
	setAttr ".tk[58]" -type "float3" 13.612395 -21.577354 -12.569736 ;
	setAttr ".tk[59]" -type "float3" 12.789778 -39.743214 -9.7305002 ;
	setAttr ".tk[60]" -type "float3" 8.3472767 5.9700599 -7.2603083 ;
	setAttr ".tk[61]" -type "float3" 6.8526168 9.9170227 -7.2405615 ;
	setAttr ".tk[62]" -type "float3" 4.2048726 -7.4047232 -5.1189752 ;
	setAttr ".tk[63]" -type "float3" 4.8507633 -10.73616 -4.610467 ;
	setAttr ".tk[64]" -type "float3" 6.2348495 -12.569236 -4.3444571 ;
	setAttr ".tk[65]" -type "float3" 9.4913273 1.2789173 -7.0839663 ;
createNode polyCloseBorder -n "polyCloseBorder1";
	rename -uid "59B32B31-4CA4-6020-8399-4990F07E670F";
	setAttr ".ics" -type "componentList" 6 "e[9]" "e[12]" "e[34]" "e[43]" "e[46]" "e[128:132]";
createNode polyTweak -n "polyTweak6";
	rename -uid "CA96153E-4FDF-2E7B-3BCE-5B8949E902C9";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[1]" -type "float3" 1.0974693 0 0 ;
	setAttr ".tk[4]" -type "float3" 1.0974693 0 0 ;
	setAttr ".tk[21]" -type "float3" -4.1279182 0 0 ;
	setAttr ".tk[24]" -type "float3" 1.0974693 0 0 ;
	setAttr ".tk[27]" -type "float3" 1.0974693 0 0 ;
	setAttr ".tk[29]" -type "float3" 0.29946345 0 0 ;
	setAttr ".tk[30]" -type "float3" 2.0250144 0 0 ;
	setAttr ".tk[31]" -type "float3" -2.293437 0 0 ;
	setAttr ".tk[45]" -type "float3" 0.29946345 0 0 ;
	setAttr ".tk[47]" -type "float3" -2.293437 0 0 ;
	setAttr ".tk[66]" -type "float3" -0.53307867 0 0 ;
	setAttr ".tk[67]" -type "float3" -0.53307867 0 0 ;
	setAttr ".tk[68]" -type "float3" -0.53307867 0 0 ;
	setAttr ".tk[69]" -type "float3" -0.53307867 0 0 ;
createNode polySplit -n "polySplit16";
	rename -uid "AC74F18E-4DE5-FE3D-F39E-FA9E3F0BE2E2";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483614 -2147483605;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit17";
	rename -uid "C6DFA1A0-461A-5B63-AD22-2BB539E9EC23";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483517 -2147483519;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit18";
	rename -uid "4E2F264B-4AF5-0325-4A01-F09A0FE6F1F6";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483614 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "837BF731-4565-9E4C-1EC5-0D9BB2905312";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[12]" -type "float3" -0.38483211 -1.4210855e-14 1.4743205 ;
	setAttr ".tk[13]" -type "float3" 0 3.5527137e-15 0.80651098 ;
	setAttr ".tk[29]" -type "float3" 0 -4.4747334 0 ;
	setAttr ".tk[31]" -type "float3" 0 -4.4747334 0 ;
	setAttr ".tk[38]" -type "float3" 0 -4.4747334 0 ;
	setAttr ".tk[42]" -type "float3" 0 3.5527137e-15 0.80651098 ;
	setAttr ".tk[45]" -type "float3" -4.2161946 -0.21702985 0 ;
createNode polySplit -n "polySplit19";
	rename -uid "ADE8B40B-49F5-C767-D9DB-75B85B39A386";
	setAttr -s 13 ".e[0:12]"  0.504246 0.495754 0.495754 0.495754 0.504246
		 0.504246 0.504246 0.495754 0.504246 0.504246 0.504246 0.504246 0.504246;
	setAttr -s 13 ".d[0:12]"  -2147483647 -2147483597 -2147483609 -2147483620 -2147483584 -2147483641 
		-2147483643 -2147483582 -2147483622 -2147483611 -2147483599 -2147483645 -2147483647;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit20";
	rename -uid "7C1829C4-412F-886A-3DBD-9E8DA367581D";
	setAttr -s 19 ".e[0:18]"  0.486182 0.51381803 0.51381803 0.51381803
		 0.51381803 0.51381803 0.51381803 0.51381803 0.486182 0.486182 0.486182 0.51381803
		 0.486182 0.486182 0.51381803 0.486182 0.486182 0.486182 0.486182;
	setAttr -s 19 ".d[0:18]"  -2147483648 -2147483596 -2147483608 -2147483592 -2147483559 -2147483563 
		-2147483551 -2147483576 -2147483579 -2147483642 -2147483644 -2147483581 -2147483570 -2147483623 -2147483567 -2147483612 -2147483600 -2147483646 
		-2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "92C0EBD6-4E91-9E45-82BB-5DA7CA6088A3";
	setAttr ".uopa" yes;
	setAttr -s 17 ".tk";
	setAttr ".tk[6]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[7]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[8]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[9]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[10]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[11]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[15]" -type "float3" 0 9.0109262 0.30909082 ;
	setAttr ".tk[20]" -type "float3" 0 7.6817322 1.154543 ;
	setAttr ".tk[26]" -type "float3" 0 -3.0641372 0.60001606 ;
	setAttr ".tk[70]" -type "float3" 0 -6.3013611 -1.9249549 ;
	setAttr ".tk[75]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[76]" -type "float3" 0 4.7683716e-07 0 ;
	setAttr ".tk[79]" -type "float3" 0 11.062352 1.5885096 ;
	setAttr ".tk[80]" -type "float3" 0 -3.8959503 1.1065587 ;
	setAttr ".tk[81]" -type "float3" 0 -8.2828016 -7.1054274e-15 ;
	setAttr ".tk[97]" -type "float3" 0 11.062348 1.5885096 ;
	setAttr ".tk[98]" -type "float3" 0 -3.8959503 1.1065587 ;
createNode polySplit -n "polySplit21";
	rename -uid "18F68AA6-446B-88D1-179B-55A471A65C9B";
	setAttr -s 7 ".e[0:6]"  0.104157 0.89584303 0.104157 0.89584303 0.104157
		 0.89584303 0.104157;
	setAttr -s 7 ".d[0:6]"  -2147483615 -2147483604 -2147483491 -2147483605 -2147483614 -2147483499 
		-2147483615;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit22";
	rename -uid "68569B6B-4833-B0CA-51DE-D4A5B5EA8ED2";
	setAttr -s 7 ".e[0:6]"  0.100806 0.899194 0.100806 0.899194 0.100806
		 0.899194 0.100806;
	setAttr -s 7 ".d[0:6]"  -2147483613 -2147483469 -2147483518 -2147483516 -2147483455 -2147483606 
		-2147483613;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "DF96B67B-4F16-9354-4416-CF9448C96097";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.38888890183580305 0 0 0 0 2.4685185343087404 0
		 53.061992443127998 -34.003405902546099 41.243634772383153 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak9";
	rename -uid "66F698C7-44BA-02D1-766F-30BB4CC2FC0D";
	setAttr ".uopa" yes;
	setAttr -s 81 ".tk";
	setAttr ".tk[0]" -type "float3" 0.14647958 -0.340069 6.4132318 ;
	setAttr ".tk[1]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[2]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[3]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[4]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[5]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[12]" -type "float3" -0.11428268 3.4298296 0.12250005 ;
	setAttr ".tk[17]" -type "float3" 0.010495647 0.17225781 -1.0381813 ;
	setAttr ".tk[18]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[19]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[20]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[21]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[22]" -type "float3" 1.4156406 9.8814144 0.76032531 ;
	setAttr ".tk[23]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[24]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[25]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[26]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[27]" -type "float3" 0 -2.9177866 6.9362898 ;
	setAttr ".tk[28]" -type "float3" 0 -2.9177866 6.9362898 ;
	setAttr ".tk[30]" -type "float3" 0.8574279 7.3767943 0.57929069 ;
	setAttr ".tk[37]" -type "float3" 0.62566775 -0.070401333 -0.23220269 ;
	setAttr ".tk[39]" -type "float3" 1.371069 -0.23128371 -0.35038641 ;
	setAttr ".tk[40]" -type "float3" 0.029645786 0.37813702 -0.038029734 ;
	setAttr ".tk[41]" -type "float3" -1.5483806 5.8275433 0.92303288 ;
	setAttr ".tk[44]" -type "float3" 1.1592189 4.9230433 -7.1054274e-15 ;
	setAttr ".tk[45]" -type "float3" 2.9770148 -2.0460792 0.18389279 ;
	setAttr ".tk[46]" -type "float3" -0.529796 -7.2324705 0.85770899 ;
	setAttr ".tk[47]" -type "float3" 2.1645465 -0.04964307 0.4937107 ;
	setAttr ".tk[48]" -type "float3" 0.83594632 -1.9534488 2.2788594 ;
	setAttr ".tk[49]" -type "float3" -3.8426843 1.0721405 1.1180228 ;
	setAttr ".tk[50]" -type "float3" -2.6596684 5.2424498 2.662225 ;
	setAttr ".tk[51]" -type "float3" 1.9688742 -0.82040364 -0.20097269 ;
	setAttr ".tk[52]" -type "float3" -2.1640122 -11.602044 0.48468333 ;
	setAttr ".tk[53]" -type "float3" -5.2777901 -5.1779828 0.36000261 ;
	setAttr ".tk[54]" -type "float3" 2.5046999 -14.812311 1.1241075 ;
	setAttr ".tk[55]" -type "float3" 3.4491723 -18.650583 1.6803445 ;
	setAttr ".tk[56]" -type "float3" 3.0298908 -11.961827 1.3025328 ;
	setAttr ".tk[57]" -type "float3" 4.0800591 -15.632418 1.7362242 ;
	setAttr ".tk[58]" -type "float3" 2.1027179 -17.797234 0.99973649 ;
	setAttr ".tk[59]" -type "float3" 2.4754407 -21.985737 1.5145203 ;
	setAttr ".tk[60]" -type "float3" -1.0605431 -15.127911 1.5802834 ;
	setAttr ".tk[61]" -type "float3" -0.48011991 -10.831771 1.8068846 ;
	setAttr ".tk[62]" -type "float3" -1.9689131 -12.802061 2.6290331 ;
	setAttr ".tk[63]" -type "float3" -3.0205808 -16.490776 2.4544084 ;
	setAttr ".tk[64]" -type "float3" -4.1386137 -21.071022 2.1029608 ;
	setAttr ".tk[65]" -type "float3" -1.7986023 -19.133551 1.4384166 ;
	setAttr ".tk[66]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[67]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[68]" -type "float3" 0 -2.9177866 6.9362898 ;
	setAttr ".tk[69]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[70]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[71]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[72]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[79]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[80]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[81]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[82]" -type "float3" 0 -6.319437 4.5045772 ;
	setAttr ".tk[83]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[84]" -type "float3" 0.22974908 0.17951027 -1.0320045 ;
	setAttr ".tk[85]" -type "float3" 1.2517765 0.72574139 0.4144963 ;
	setAttr ".tk[86]" -type "float3" 2.5422516 -0.60330808 1.0353477 ;
	setAttr ".tk[87]" -type "float3" 1.5856072 -4.9802527 1.5505739 ;
	setAttr ".tk[88]" -type "float3" 1.0253046 -6.6124344 0.01492858 ;
	setAttr ".tk[89]" -type "float3" 0.35235563 0.14508048 -0.053184256 ;
	setAttr ".tk[95]" -type "float3" 0.12490514 1.2870086 0.019677386 ;
	setAttr ".tk[96]" -type "float3" 0.18395405 9.5888119 0.44601643 ;
	setAttr ".tk[97]" -type "float3" 0 0 -1.1111262 ;
	setAttr ".tk[98]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[99]" -type "float3" 0 -8.2208958 6.3684688 ;
	setAttr ".tk[100]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[101]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[102]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[103]" -type "float3" 0.40918013 0 6.3684688 ;
	setAttr ".tk[104]" -type "float3" 0.52740812 6.1788449 7.4229937 ;
	setAttr ".tk[105]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[106]" -type "float3" 0 6.1788449 7.4229937 ;
	setAttr ".tk[107]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[108]" -type "float3" -0.60465688 6.1788449 7.4229937 ;
	setAttr ".tk[109]" -type "float3" -0.27192551 0 6.3684688 ;
	setAttr ".tk[110]" -type "float3" 0 0 6.3684688 ;
	setAttr ".tk[111]" -type "float3" 0 0 6.3684688 ;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "D4ECB88D-4030-5B97-CDBD-8F92D15A0223";
	setAttr ".ax" -type "double3" 0 1 0 ;
	setAttr ".r" 0.75;
	setAttr ".h" 12;
	setAttr ".sa" 12;
	setAttr ".sh" 14;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "F08FF79B-4D2F-7E49-8578-29AB65C9CF63";
	setAttr ".dc" -type "componentList" 6 "e[361]" "e[363]" "e[365]" "e[367]" "e[369]" "e[371]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "24D33F17-4E85-BF4F-4EC9-A989870C4CB0";
	setAttr ".dc" -type "componentList" 6 "e[349]" "e[351]" "e[353]" "e[355]" "e[357]" "e[359]";
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "CC3ECF9F-47E0-1E3B-056D-D7859745825E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 0.97881992492062253 0 0 0 0 1 0 0 0 0 1 0 0 -150.19204492241499 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak10";
	rename -uid "E99E4840-4C99-DB51-3063-108D055D2FD5";
	setAttr ".uopa" yes;
	setAttr -s 182 ".tk";
	setAttr ".tk[0:165]" -type "float3"  -19.038146973 4.43479681 10.99168015
		 -10.99168015 4.43479681 19.038146973 0 4.43479681 21.98336029 10.99168015 4.43479681
		 19.038146973 19.038146973 4.43479681 10.99168015 21.98336029 4.43479681 0 19.038146973
		 4.43479681 -10.99168015 10.99168015 4.43479681 -19.038146973 0 4.43479681 -21.98336029
		 -10.99168015 4.43479681 -19.038146973 -19.038146973 4.43479681 -10.99168015 -21.98336029
		 4.43479681 0 -18.056732178 -13.45035172 10.4250555 -10.4250555 -13.45035172 18.056732178
		 0 -13.45035172 20.85011101 10.4250555 -13.45035172 18.056732178 18.056732178 -13.45035172
		 10.4250555 20.85011101 -13.45035172 0 18.056732178 -13.45035172 -10.4250555 10.4250555
		 -13.45035172 -18.056732178 0 -13.45035172 -20.85011101 -10.4250555 -13.45035172 -18.056732178
		 -18.056732178 -13.45035172 -10.4250555 -20.85011101 -13.45035172 0 -17.56892204 -29.50873947
		 10.14342499 -10.14342499 -29.50873947 17.56892204 0 -29.50873947 20.28684998 10.14342499
		 -29.50873947 17.56892204 17.56892204 -29.50873947 10.14342499 20.28684998 -29.50873947
		 0 17.56892204 -29.50873947 -10.14342499 10.14342499 -29.50873947 -17.56892204 0 -29.50873947
		 -20.28684998 -10.14342499 -29.50873947 -17.56892204 -17.56892204 -29.50873947 -10.14342499
		 -20.28684998 -29.50873947 0 -18.12279129 -43.71434784 10.46319962 -10.46319962 -43.71434784
		 18.12279129 0 -43.71434784 20.92639923 10.46319962 -43.71434784 18.12279129 18.12279129
		 -43.71434784 10.46319962 20.92639923 -43.71434784 0 18.12279129 -43.71434784 -10.46319962
		 10.46319962 -43.71434784 -18.12279129 0 -43.71434784 -20.92639923 -10.46319962 -43.71434784
		 -18.12279129 -18.12279129 -43.71434784 -10.46319962 -20.92639923 -43.71434784 0 -15.99856472
		 -43.24225235 9.23677349 -9.23677349 -43.24225235 15.99856472 0 -43.24225235 18.47354698
		 9.23677349 -43.24225235 15.99856472 15.99856472 -43.24225235 9.23677349 18.47354698
		 -43.24225235 0 15.99856472 -43.24225235 -9.23677349 9.23677349 -43.24225235 -15.99856472
		 0 -43.24225235 -18.47354698 -9.23677349 -43.24225235 -15.99856472 -15.99856472 -43.24225235
		 -9.23677349 -18.47354698 -43.24225235 0 -16.99816895 -45.69113922 9.81389904 -9.81389904
		 -45.69113922 16.99816895 0 -45.69113922 19.62779808 9.81389904 -45.69113922 16.99816895
		 16.99816895 -45.69113922 9.81389904 19.62779808 -45.69113922 0 16.99816895 -45.69113922
		 -9.81389904 9.81389904 -45.69113922 -16.99816895 0 -45.69113922 -19.62779808 -9.81389904
		 -45.69113922 -16.99816895 -16.99816895 -45.69113922 -9.81389904 -19.62779808 -45.69113922
		 0 -17.80950356 -59.76190186 10.28231716 -10.28231716 -59.76190186 17.80950356 0 -59.76190186
		 20.56463432 10.28231716 -59.76190186 17.80950356 17.80950356 -59.76190186 10.28231716
		 20.56463432 -59.76190186 0 17.80950356 -59.76190186 -10.28231716 10.28231716 -59.76190186
		 -17.80950356 0 -59.76190186 -20.56463432 -10.28231716 -59.76190186 -17.80950356 -17.80950356
		 -59.76190186 -10.28231716 -20.56463432 -59.76190186 0 -17.62414551 -77.99302673 10.17530155
		 -10.17530155 -77.99302673 17.62414551 0 -77.99302673 20.3506031 10.17530155 -77.99302673
		 17.62414551 17.62414551 -77.99302673 10.17530155 20.3506031 -77.99302673 -4.3334127e-09
		 17.62414551 -77.99302673 -10.17530155 10.17530155 -77.99302673 -17.62414551 0 -77.99302673
		 -20.3506031 -10.17530155 -77.99302673 -17.62414551 -17.62414551 -77.99302673 -10.17530155
		 -20.3506031 -77.99302673 -4.3334127e-09 -16.15555573 -91.26921082 9.32741165 -9.32741165
		 -91.26921082 16.15555573 0 -91.26921082 18.6548233 9.32741165 -91.26921082 16.15555573
		 16.15555573 -91.26921082 9.32741165 18.6548233 -91.26921082 -1.9162204e-08 16.15555573
		 -91.26921082 -9.32741165 9.32741165 -91.26921082 -16.15555573 0 -91.26921082 -18.6548233
		 -9.32741165 -91.26921082 -16.15555573 -16.15555573 -91.26921082 -9.32741165 -18.6548233
		 -91.26921082 -1.9162204e-08 -11.089086533 -60.91511536 6.4022913 -6.4022913 -60.91511536
		 11.089086533 0 -60.91511536 12.8045826 6.4022913 -60.91511536 11.089086533 11.089086533
		 -60.91511536 6.4022913 12.8045826 -60.91511536 9.7626305e-08 11.089086533 -60.91511536
		 -6.4022913 6.4022913 -60.91511536 -11.089086533 0 -60.91511536 -12.8045826 -6.4022913
		 -60.91511536 -11.089086533 -11.089086533 -60.91511536 -6.4022913 -12.8045826 -60.91511536
		 9.7626305e-08 -11.24781418 -45.40200043 5.29407406 -6.49392891 -45.40200043 10.047958374
		 0 -45.40200043 11.78800201 6.49392891 -45.40200043 10.047958374 11.24781418 -45.40200043
		 5.29407406 12.98785782 -45.40200043 -1.19985545 11.24781418 -45.40200043 -7.69378376
		 6.49392891 -45.40200043 -12.44766998 0 -45.40200043 -14.18771362 -6.49392891 -45.40200043
		 -12.44766998 -11.24781418 -45.40200043 -7.69378376 -12.98785782 -45.40200043 -1.19985545
		 -12.69684315 -31.20372963 4.19747305 -7.33052492 -31.20372963 9.56379128 0 -31.20372963
		 11.52799797 7.33052492 -31.20372963 9.56379128 12.69684315 -31.20372963 4.19747305
		 14.66104984 -31.20372963 -3.13305211 12.69684315 -31.20372963 -10.46357632 7.33052492
		 -31.20372963 -15.82989502 0 -31.20372963 -17.79410553 -7.33052492 -31.20372963 -15.82989502
		 -12.69684315 -31.20372963 -10.46357632 -14.66104984 -31.20372963 -3.13305211 -7.83664322
		 -0.88850486 1.21484101 -4.52448845 -0.88850486 4.53149605 0 -0.88850486 5.74549866
		 4.52448845 -0.88850486 4.53149605 7.83664322 -0.88850486 1.21484101 9.048976898 -0.88850486
		 -3.31566906 7.83664322 -0.88850486 -7.84602261 4.52448845 -0.88850486 -11.16236782
		 0 -0.88850486 -12.37621307 -4.52448845 -0.88850486 -11.16236782 -7.83664322 -0.88850486
		 -7.84602261 -9.048976898 -0.88850486 -3.31566906 -3.28445506 -13.92765331 1.89628124
		 -1.89628124 -13.92765331 3.28445506 0 -13.92765331 3.79256248 1.89628124 -13.92765331
		 3.28445506 3.28445506 -13.92765331 1.89628124 3.79256248 -13.92765331 7.3721989e-09
		 3.28445506 -13.92765331 -1.89628124 1.89628124 -13.92765331 -3.28445506 0 -13.92765331
		 -3.79256248 -1.89628124 -13.92765331 -3.28445506;
	setAttr ".tk[166:181]" -3.28445506 -13.92765331 -1.89628124 -3.79256248 -13.92765331
		 7.3721989e-09 -0.81445819 -4.13049221 0.47022769 -0.47022769 -4.13049221 0.81445819
		 0 -4.13049221 0.94045538 0.47022769 -4.13049221 0.81445819 0.81445819 -4.13049221
		 0.47022769 0.94045538 -4.13049221 0 0.81445819 -4.13049221 -0.47022769 0.47022769
		 -4.13049221 -0.81445819 0 -4.13049221 -0.94045538 -0.47022769 -4.13049221 -0.81445819
		 -0.81445819 -4.13049221 -0.47022769 -0.94045538 -4.13049221 0 0 -1.85964131 0 0 10.27753258
		 9.56776333;
createNode polyTweak -n "polyTweak11";
	rename -uid "A4DAF9A0-4953-107C-F2F2-93B0D353D320";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk[0:17]" -type "float3"  12.093289375 -19.39706802
		 11.62057114 0 -19.39706802 11.62057114 -12.093289375 -19.39706802 11.62057114 12.093289375
		 -31.84324265 14.55655289 0 -31.84324265 14.55655289 -12.093289375 -31.84324265 14.55655289
		 2.55989671 -3.11685705 10.58224678 0 -3.11685705 10.58224678 -2.55989671 -3.11685705
		 10.58224678 0 1.67088032 4.17720079 0 1.67088032 4.17720079 0 1.67088032 4.17720079
		 0 5.56960106 -2.50632048 0 5.56960106 -2.50632048 0 5.56960106 -2.50632048 2.55989671
		 6.73709774 4.17720079 0 6.73709774 4.17720079 -2.55989671 6.73709774 4.17720079;
createNode polySplit -n "polySplit23";
	rename -uid "E55ECC29-4253-8B53-20EC-F4BD49B29BFC";
	setAttr -s 7 ".e[0:6]"  0.50607699 0.50607699 0.50607699 0.49392301
		 0.49392301 0.49392301 0.50607699;
	setAttr -s 7 ".d[0:6]"  -2147483630 -2147483629 -2147483628 -2147483622 -2147483623 -2147483624 
		-2147483630;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "E2F3DE03-47E2-309F-137B-F9B88B542966";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[18:23]" -type "float3"  0 2.99482322 1.61259723 0
		 2.99482322 1.61259723 0 2.99482322 1.61259723 0 1.94936037 0.27848005 0 1.94936037
		 0.27848005 0 1.94936037 0.27848005;
createNode polySplit -n "polySplit24";
	rename -uid "476658C2-43D1-AD0B-0865-7AB503F6E389";
	setAttr -s 7 ".e[0:6]"  0.42026201 0.42026201 0.42026201 0.57973802
		 0.57973802 0.57973802 0.42026201;
	setAttr -s 7 ".d[0:6]"  -2147483633 -2147483632 -2147483631 -2147483619 -2147483620 -2147483621 
		-2147483633;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak13";
	rename -uid "DF55ACA1-412A-0FD2-0493-4AB24AC43C19";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[24:29]" -type "float3"  0 3.41536999 2.46665597 0
		 3.41536999 2.46665597 0 3.41536999 2.46665597 0 0.75897098 -2.087170362 0 0.75897098
		 -2.087170362 0 0.75897098 -2.087170362;
createNode polySplit -n "polySplit25";
	rename -uid "B70CCE0B-49EF-C133-1EB5-5B8F4D10197E";
	setAttr -s 7 ".e[0:6]"  0.490116 0.490116 0.490116 0.509884 0.509884
		 0.509884 0.490116;
	setAttr -s 7 ".d[0:6]"  -2147483621 -2147483620 -2147483619 -2147483602 -2147483603 -2147483604 
		-2147483621;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit26";
	rename -uid "9CE0B3F1-4C4E-F48B-DECD-F89A7AF66A82";
	setAttr -s 7 ".e[0:6]"  0.48564199 0.48564199 0.48564199 0.51435798
		 0.51435798 0.51435798 0.48564199;
	setAttr -s 7 ".d[0:6]"  -2147483633 -2147483632 -2147483631 -2147483601 -2147483600 -2147483599 
		-2147483633;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweak -n "polyTweak14";
	rename -uid "97C628CF-40B7-FC11-1E6C-F7AE7C942A4D";
	setAttr ".uopa" yes;
	setAttr -s 39 ".tk[3:41]" -type "float3"  0 2.087170362 -0.18974286
		 0 2.087170362 -0.18974286 0 2.087170362 -0.18974286 0 -3.72329831 0 0 1.241099 0
		 0 -3.72329831 0 0 -3.72329831 0 0 1.241099 0 0 -3.72329831 0 -8.9746685 9.80126476
		 0 0 9.80126476 0 8.9746685 9.80126476 0 -2.81780052 7.39996481 1.1920929e-07 5.8280125e-07
		 7.39996481 1.1920929e-07 2.81780148 7.39996481 1.1920929e-07 0 -3.72329831 0 0 1.241099
		 0 0 -3.72329831 0 8.23007679 9.042295456 -0.75897086 0 9.042295456 -0.75897086 -8.23007679
		 9.042295456 -0.75897086 0 2.087170362 -0.18974286 0 2.087170362 -0.18974286 0 2.087170362
		 -0.18974286 6.60747099 7.2141819 0 -2.0464265e-07 7.2141819 0 -6.60747099 7.2141819
		 0 0 6.64099455 3.9845984 0 6.64099455 3.9845984 0 6.64099455 3.9845984 0 -2.46665668
		 -1.1920929e-07 0 -2.46665668 -1.1920929e-07 0 -2.46665668 -1.1920929e-07 0 -1.13845634
		 -1.51794171 0 -1.13845634 -1.51794171 0 -1.13845634 -1.51794171 0 6.45125198 -1.13845658
		 0 6.45125198 -1.13845658 0 6.45125198 -1.13845658;
createNode polySplit -n "polySplit27";
	rename -uid "E6027D77-49B9-B1C0-8069-E88353EC0FE9";
	setAttr -s 17 ".e[0:16]"  0.436921 0.436921 0.436921 0.563079 0.563079
		 0.436921 0.436921 0.563079 0.563079 0.563079 0.563079 0.436921 0.436921 0.563079
		 0.436921 0.436921 0.436921;
	setAttr -s 17 ".d[0:16]"  -2147483636 -2147483635 -2147483634 -2147483572 -2147483596 -2147483584 
		-2147483618 -2147483608 -2147483625 -2147483626 -2147483627 -2147483605 -2147483617 -2147483581 -2147483593 -2147483569 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "refImgs_lyr01.di" "imagePlane1.do";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":frontShape.msg" "imagePlaneShape1.ltc";
connectAttr "refImgs_lyr01.di" "imagePlane2.do";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape2.ws";
connectAttr ":sideShape.msg" "imagePlaneShape2.ltc";
connectAttr "polySplit27.out" "skull0Shape1.i";
connectAttr "polySoftEdge2.out" "body0Shape1.i";
connectAttr "polySoftEdge1.out" "l_arm0Shape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[1]" "refImgs_lyr01.id";
connectAttr "|l_arm_grp01|l_arm01|polySurfaceShape1.o" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "polySplit9.out" "polySplit10.ip";
connectAttr "polySplit10.out" "polySplit11.ip";
connectAttr "polySplit11.out" "polySplit12.ip";
connectAttr "polySplit12.out" "polySplit13.ip";
connectAttr "polySplit13.out" "polySplit14.ip";
connectAttr "polySplit14.out" "polyExtrudeFace1.ip";
connectAttr "l_arm0Shape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak3.out" "polyExtrudeFace2.ip";
connectAttr "l_arm0Shape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak3.ip";
connectAttr "polyExtrudeFace2.out" "polyTweak4.ip";
connectAttr "polyTweak4.out" "polySplit15.ip";
connectAttr "polyTweak5.out" "polySplitEdge1.ip";
connectAttr "polySplit15.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyCloseBorder1.ip";
connectAttr "polySplitEdge1.out" "polyTweak6.ip";
connectAttr "polyCloseBorder1.out" "polySplit16.ip";
connectAttr "polySplit16.out" "polySplit17.ip";
connectAttr "polySplit17.out" "polySplit18.ip";
connectAttr "polySplit18.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "polySplit19.ip";
connectAttr "polySplit19.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polyTweak8.ip";
connectAttr "polyTweak8.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polySplit22.ip";
connectAttr "polyTweak9.out" "polySoftEdge1.ip";
connectAttr "l_arm0Shape1.wm" "polySoftEdge1.mp";
connectAttr "polySplit22.out" "polyTweak9.ip";
connectAttr "polyCylinder1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "polyTweak10.out" "polySoftEdge2.ip";
connectAttr "body0Shape1.wm" "polySoftEdge2.mp";
connectAttr "deleteComponent2.og" "polyTweak10.ip";
connectAttr "polyCube1.out" "polyTweak11.ip";
connectAttr "polyTweak11.out" "polySplit23.ip";
connectAttr "polySplit23.out" "polyTweak12.ip";
connectAttr "polyTweak12.out" "polySplit24.ip";
connectAttr "polySplit24.out" "polyTweak13.ip";
connectAttr "polyTweak13.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polyTweak14.ip";
connectAttr "polyTweak14.out" "polySplit27.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "skull0Shape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "l_arm0Shape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "body0Shape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "r_arm0Shape1.iog" ":initialShadingGroup.dsm" -na;
// End of badGuy_Model01.ma
