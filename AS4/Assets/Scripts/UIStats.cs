﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStats : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public Transform mainCam;
    public Motor motor;

    public Text hpText;
    public Text attackText;
    public Text stepText;

    //[Header("Prefabs")]
 
    void Awake()
    {
        mainCam = Camera.main.transform;
        motor = GetComponentInParent<Motor>();
    }
}