﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public GameObject audioSourcePrefab;

    public AudioClip healClip;

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    public void PlayAudio(AudioClip clip, float volume = 1)
    {
        StartCoroutine(PlayAudioCoroutine(clip, volume));
    }

    IEnumerator PlayAudioCoroutine(AudioClip audioClip, float volume)
    {
        GameObject audioSourceInstance = Instantiate(audioSourcePrefab, transform);
        AudioSource audioSource = audioSourceInstance.GetComponent<AudioSource>();
        audioSource.clip = audioClip;
        audioSource.volume = volume;
        audioSource.Play();
        yield return new WaitForSeconds(audioClip.length);
        Destroy(audioSourceInstance);
    }

    public void Heal()
    {
        PlayAudio(healClip);
    }
}
