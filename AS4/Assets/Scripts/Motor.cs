﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterType { Attacker, Healer, Enemy }
public class Motor : MonoBehaviour
{
    [Header("Settings")]
    public float speed = 5;
    //[Tooltip("yes if radial, no if linear")] public bool isNormalRadial;
    public int normalAttackDistance;
    //[Tooltip("yes if radial, no if linear")] public bool isSpecialRadial;
    public int specialAttackDistance;
    public int maxSpecialAttackCount;
    public int maxAttackCount;
    public int maxStepCount;
    public int maxHealth;
    public int normalAttackDamage;
    public int specialAttackDamage;
    //public int characterId;
    public CharacterType characterType;

    [Header("References")]
    public cakeslice.Outline outline; 
    public LayerMask nodeLayer;
    public float timeLimit;
    public float timer = 0;
    public Coroutine movementCoroutine;
    public int remainingSpecialAttackCount;
    public int remainingStepCount;
    public int remainingHealth;
    public int remainingAttackCount;

    public Transform weaponHandTransform;
    public Transform weaponHolsterTransform;
    public Transform revolverTransform;

    //void Start()
    //{
    //    outline.enabled = false;
    //}

    public void EquipRevolver()
    {
        revolverTransform.parent = weaponHandTransform;
    }
    public void HolsterRevolver()
    {
        revolverTransform.parent = weaponHolsterTransform;
    }

    void Update()
    {
        revolverTransform.localPosition = Vector3.zero;
        revolverTransform.localRotation = Quaternion.identity;
    }

    public void TakeDamage(int damage)
    {
        remainingHealth -= damage;
        Animator anim = GetComponentInChildren<Animator>();

        if (remainingHealth <= 0)
        {
            if (this != null)
            {
                DeathCoroutine();
            }
        }
    }

    void DeathCoroutine()
    {
        //TODO Death Animation
        //GetComponentInChildren<Animator>().SetTrigger("Death");
        //yield return new WaitForSeconds(2.1f);
        RemoveCurrentPositionNode();
        Destroy(gameObject);
    }

    public void Heal(int healAmount)
    {
        remainingHealth = Mathf.Clamp(remainingHealth + healAmount, 0, maxHealth);
    }

    public IEnumerator ProcessMovement(Vector3[] points)
    {
        CharacterController.instance.characterOptionsCanvas.SetActive(false);
        CharacterController.instance.mainOptionsCanvas.SetActive(false);
        CharacterController.instance.inCombat = true;
        Animator anim = GetComponentInChildren<Animator>();
        anim.SetBool("Move", true);
        RemoveCurrentPositionNode();
        for (int i = 0; i < points.Length; i++)
        {
            timeLimit = 1 / speed;
            while (Vector3.Distance(transform.position, points[i]) > .1f)
            {
                transform.rotation = Quaternion.LookRotation(points[i] - transform.position);
                timer += Time.deltaTime;
                transform.position = Vector3.Lerp(transform.position, points[i], timer / timeLimit);
                yield return null;
            }
            transform.position = points[i];
            timer = 0;
        }
        SetCurrentPositionNode();
        anim.SetBool("Move", false);
        CharacterController.instance.inCombat = false;
        //CharacterController.instance.characterOptionsCanvas.SetActive(true);
        //CharacterController.instance.mainOptionsCanvas.SetActive(true);
    }

    public virtual void SetCurrentPositionNode()
    {

    }

    public virtual void RemoveCurrentPositionNode()
    {

    }

    public virtual void NewRound()
    {
        remainingAttackCount = maxAttackCount;
        remainingStepCount = maxStepCount;
    }
}