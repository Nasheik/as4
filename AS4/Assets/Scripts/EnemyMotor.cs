﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMotor : Motor
{
    public CharacterMotor targetCharacterMotor;

    void Start()
    {
        remainingStepCount = maxStepCount;
        remainingHealth = maxHealth;
        SetCurrentPositionNode();
        LevelManager.instance.OnNewRound += NewRound;
        LevelManager.instance.allEnemies.Add(this);
        outline.enabled = false;
    }

    public void MoveCharacter(List<Vector3> path)
    {
        if (movementCoroutine != null)
        {
            StopCoroutine(movementCoroutine);
        }
        movementCoroutine = StartCoroutine(ProcessMovement(path.ToArray()));
    }

    public override void SetCurrentPositionNode()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 1.25f, nodeLayer))
        {
            Node currentPositionNode = hit.collider.gameObject.GetComponent<Node>();
            currentPositionNode.isOccupied = true;
            currentPositionNode.isEnemy = true;
            currentPositionNode.unit = gameObject;
        }
    }

    public override void RemoveCurrentPositionNode()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up, -transform.up, out hit, 2f, nodeLayer))
        {
            Node currentPositionNode = hit.collider.gameObject.GetComponent<Node>();
            currentPositionNode.isOccupied = false;
            currentPositionNode.isEnemy = false;
            currentPositionNode.unit = null;
        }
    }

    void OnDestroy()
    {
        LevelManager.instance.allEnemies.Remove(this);
    }
}