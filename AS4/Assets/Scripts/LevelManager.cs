﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [Header("References")]
    public GameObject bg;
    public Text youWinText;
    public Text timeScaleText;
    public GameObject optionsHolder;
    public GameObject endTurnBackground;
    public GameObject endTurnButton;
    public GameObject selectedCanvas;
    public GameObject targetCanvas;

    [Header("Private")]
    public List<CharacterMotor> allCharacters = new List<CharacterMotor>();
    public List<EnemyMotor> allEnemies = new List<EnemyMotor>();
    public static LevelManager instance;
    public event Action OnNewRound;
    Vector3 prevPos;
    bool scaleTime;

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    public bool pauseMenuIsUp;
    void Update()
    {
        CheckAndProcessFailState();
        CheckAndProcessWinState();//TODO Take out of update

        if (Input.GetKeyDown(KeyCode.F))
        {
            OnCallbackScaleTime();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenuIsUp = !pauseMenuIsUp;
            if (pauseMenuIsUp) CallbackOnGamePaused();
            else CallbackOnGameUnpaused();
        }
    }

    public void CheckAndProcessWinState()
    {
        if (allEnemies.Count <= 0)
        {
            CharacterController.instance.characterOptionsCanvas.SetActive(false);
            CharacterController.instance.mainOptionsCanvas.SetActive(false);
            bg.SetActive(true);
            youWinText.gameObject.SetActive(true);
            youWinText.text = "You Win";
            optionsHolder.SetActive(true);
            endTurnButton.SetActive(false);
            endTurnBackground.SetActive(false);
            selectedCanvas.SetActive(false);
            targetCanvas.SetActive(false);
        }
    }

    public void CheckAndProcessFailState()
    {
        if (allCharacters.Count <= 0)
        {
            CharacterController.instance.characterOptionsCanvas.SetActive(false);
            CharacterController.instance.mainOptionsCanvas.SetActive(false);
            bg.SetActive(true);
            youWinText.gameObject.SetActive(true);
            youWinText.text = "You Lose";
            optionsHolder.SetActive(true);
            endTurnButton.SetActive(false);
            endTurnBackground.SetActive(false);
            selectedCanvas.SetActive(false);
            targetCanvas.SetActive(false);
        }
    }

    public void CallbackOnGamePaused()
    {
        bg.SetActive(true);
        youWinText.gameObject.SetActive(true);
        youWinText.text = "Paused";
        optionsHolder.SetActive(true);
        endTurnButton.SetActive(false);
        endTurnBackground.SetActive(false);
        selectedCanvas.SetActive(false);
        targetCanvas.SetActive(false);
    }

    public void CallbackOnGameUnpaused()
    {
        bg.SetActive(false);
        youWinText.gameObject.SetActive(false);
        optionsHolder.SetActive(false);
        endTurnButton.SetActive(true);
        endTurnBackground.SetActive(true);
        selectedCanvas.SetActive(true);
        targetCanvas.SetActive(true);
    }

    public void CallbackOnGameExited()
    {
        Application.Quit();
    }

    public void OnCallbackLevelSelected(int levelNumber)
    {
        SceneManager.LoadScene(levelNumber);
    }

    public void OnNewRoundWrapper()
    {
        OnNewRound();
    }

    public void OnCallbackEndTurn()
    {
        CharacterController.instance.mainOptionsCanvas.SetActive(false);
        StartCoroutine(EnemyController.instance.ProcessAI(allEnemies));
    }

    public void OnCallbackScaleTime()
    {
        if (!scaleTime)
        {
            timeScaleText.text = "10x";
            Time.timeScale = 10;
            scaleTime = true;
        }
        else
        {
            timeScaleText.text = "1x";
            Time.timeScale = 1;
            scaleTime = false;
        }
    }
}