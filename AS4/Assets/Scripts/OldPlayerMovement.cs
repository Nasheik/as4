﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OldPlayerMovement : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    NavMeshAgent agent;
    RaycastHit hit;
    Camera mainCamera;
    LineRenderer lineRenderer;

    //[Header("Prefabs")]

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = true;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
    }

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        ShowCurrentPath();
        if (Input.GetMouseButton(0))
        {
            agent.isStopped = false;
        }
    }

    void ShowCurrentPath()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        NavMeshPath travelPath = new NavMeshPath();
        if (Physics.Raycast(ray, out hit))
        {
            agent.CalculatePath(ConvertPointToTilePoint(hit.point), travelPath);
            print(agent.path.corners.Length);
        }

        for (int i = 0; i < travelPath.corners.Length; i++)
        {
            //travelPath.corners[i] = ConvertPointToTilePoint(travelPath.corners[i]);
            //print(i+"    "+travelPath.corners[i]);
        }

        //agent.SetPath(travelPath);

        if (agent.hasPath)
        {
            lineRenderer.positionCount = agent.path.corners.Length;
            lineRenderer.SetPositions(agent.path.corners);
            lineRenderer.enabled = true;
        }
        else
        {
            lineRenderer.enabled = false;
        }
    }

    Vector3 ConvertPointToTilePoint(Vector3 point)
    {
        float newX = ConvertFloatToTileFloat(point.x);
        float newZ = ConvertFloatToTileFloat(point.z);
        Vector3 newPoint = new Vector3(newX, point.y, newZ);
        return newPoint;
    }

    float ConvertFloatToTileFloat(float num)
    {
        if (num >= 0)
        {
            return (int)num + .5f;
        }
        else
        {
            return (int)num - .5f;
        }
    }
}