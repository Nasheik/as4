﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    public static PopupManager instance;
    public Animator anim;
    public Text text; 
 
    //[Header("Prefabs")]
 
    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
        anim = GetComponent<Animator>();
        text = GetComponentInChildren<Text>();
    }

    public void Popup(string msg)
    {
        //text.text = msg;
        //anim.SetTrigger("Popup");
    }
}