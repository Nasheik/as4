﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Controller
{
    //[Header("Settings")]

    [Header("References")]
    public static EnemyController instance;
    //public EnemyMotor selectedEnemyMotor;

    //[Header("Prefabs")]

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    public IEnumerator ProcessAI(List<EnemyMotor> enemies)
    {
        CharacterController.instance.inCombat = true;
        foreach (EnemyMotor enemy in enemies)
        {
            CharacterMotor targetInRange = IsNextToTarget(enemy.transform.position, enemy.targetCharacterMotor);
            if (enemy.remainingAttackCount > 0 && targetInRange != null)
            {
                rotationReference.forward = targetInRange.gameObject.transform.position - enemy.transform.position;
                rotationReference.forward = rotationReference.right;
                if (enemy.transform.forward != rotationReference.forward)
                {
                    yield return StartCoroutine(RotateObject(enemy.transform, rotationReference, .5f)); 
                }
                enemy.GetComponentInChildren<Animator>().SetTrigger("PistolShot");
                yield return new WaitForSeconds(1.06f); //Hardcoded for PistolShot
                targetInRange.GetComponentInChildren<Animator>().SetTrigger("Hit");
                yield return new WaitForSeconds(.75f); //Hardcoded for HitBody
                rotationReference.forward = targetInRange.gameObject.transform.position - enemy.transform.position;
                EnemyAttack(enemy, enemy.targetCharacterMotor);
                yield return new WaitForSeconds(0.583f); //Hardcoded for HitBody
                yield return new WaitForSeconds(1.04f); //Hardcoded for PistolShot
                yield return StartCoroutine(RotateObject(enemy.transform, rotationReference, .5f));
                continue;
            }

            //print("hi");
            if (enemy.targetCharacterMotor == null)
            {
                if (LevelManager.instance.allCharacters.Count > 0)
                {
                    enemy.targetCharacterMotor = LevelManager.instance.allCharacters[Random.Range(0, LevelManager.instance.allCharacters.Count)];
                }
                else
                {
                    continue;
                }
            }

            if (enemy.movementCoroutine != null)
            {
                StopCoroutine(enemy.movementCoroutine);
            }
            enemy.movementCoroutine = StartCoroutine(enemy.ProcessMovement(GetPath(enemy).ToArray()));
            //print("before yield");
            yield return enemy.movementCoroutine;
            CharacterController.instance.inCombat = true;
            //print("after yield");
            targetInRange = IsNextToTarget(enemy.transform.position, enemy.targetCharacterMotor);
            if (enemy.remainingAttackCount > 0 && targetInRange != null)
            {
                rotationReference.forward = targetInRange.gameObject.transform.position - enemy.transform.position;
                rotationReference.forward = rotationReference.right;
                if (enemy.transform.forward != rotationReference.forward)
                {
                    yield return StartCoroutine(RotateObject(enemy.transform, rotationReference, .5f));
                }
                enemy.GetComponentInChildren<Animator>().SetTrigger("PistolShot");
                yield return new WaitForSeconds(.15f); //Hardcoded for PistolShot
                enemy.EquipRevolver();
                yield return new WaitForSeconds(.91f); //Hardcoded for PistolShot
                targetInRange.GetComponentInChildren<Animator>().SetTrigger("Hit");
                yield return new WaitForSeconds(.75f); //Hardcoded for HitBody
                rotationReference.forward = targetInRange.gameObject.transform.position - enemy.transform.position;
                EnemyAttack(enemy, enemy.targetCharacterMotor);
                yield return new WaitForSeconds(0.583f); //Hardcoded for HitBody
                enemy.HolsterRevolver();
                yield return new WaitForSeconds(1.04f); //Hardcoded for PistolShot
                yield return StartCoroutine(RotateObject(enemy.transform, rotationReference, .5f));
            }
        }
        CharacterController.instance.inCombat = false;
        LevelManager.instance.OnNewRoundWrapper();
    }


    void EnemyAttack(EnemyMotor enemy, CharacterMotor character)
    {
        character.TakeDamage(enemy.normalAttackDamage);
        enemy.remainingAttackCount--;
    }


    public List<Vector3> GetPath(EnemyMotor enemy)
    {
        Vector3 targetSpot = FindNodeNearTarget(enemy.targetCharacterMotor.transform.position, enemy.transform.position);
        bool exists = Pathfinding.instance.CheckPathExistsAndSet(enemy.transform.position, targetSpot, true, true);
        List<Vector3> path = new List<Vector3>();
        if (exists)
        {
            path = Pathfinding.instance.mapGrid.GetTravelPath().ToList<Vector3>();
            //print(path.Count);
            int steps = Mathf.Clamp(path.Count, 0, enemy.remainingStepCount + 1);
            path = path.GetRange(0, steps);
        }
        else
        {
            path.Add(enemy.transform.position);
        }
        return path;
    }

    public CharacterMotor IsNextToTarget(Vector3 enemy, CharacterMotor target)
    {
        List<Node> nodes = new List<Node>();
        if ((int)enemy.x + 1 < Pathfinding.instance.mapGrid.width)
        {
            nodes.Add(Grid.instance.grid[(int)enemy.x + 1, (int)enemy.z]);
        }
        if ((int)enemy.x - 1 >= 0)
        {
            nodes.Add(Grid.instance.grid[(int)enemy.x - 1, (int)enemy.z]);
        }
        if ((int)enemy.z + 1 < Pathfinding.instance.mapGrid.depth)
        {
            nodes.Add(Grid.instance.grid[(int)enemy.x, (int)enemy.z + 1]);
        }
        if ((int)enemy.z - 1 >= 0)
        {
            nodes.Add(Grid.instance.grid[(int)enemy.x, (int)enemy.z - 1]);
        }

        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].isCharacter)
            {
                CharacterMotor cm = nodes[i].unit.GetComponent<CharacterMotor>();
                if (cm == target) return cm;
            }
        }
        return null;
    }

    public Vector3 FindNodeNearTarget(Vector3 target, Vector3 defaultCase)
    {
        List<Node> nodes = new List<Node>();
        if ((int)target.x + 1 < Pathfinding.instance.mapGrid.width)//right
        {
            if (!Grid.instance.grid[(int)target.x + 1, (int)target.z].isOccupied)
            {
                nodes.Add(Grid.instance.grid[(int)target.x + 1, (int)target.z]);
            }
        }
        if ((int)target.x - 1 >= 0)//left
        {
            if (!Grid.instance.grid[(int)target.x - 1, (int)target.z].isOccupied)
            {
                nodes.Add(Grid.instance.grid[(int)target.x - 1, (int)target.z]);
            }
        }
        if ((int)target.z + 1 < Pathfinding.instance.mapGrid.depth)//up
        {
            if (!Grid.instance.grid[(int)target.x, (int)target.z + 1].isOccupied)
            {
                nodes.Add(Grid.instance.grid[(int)target.x, (int)target.z + 1]);
            }
        }
        if ((int)target.z - 1 >= 0)//down
        {
            if (!Grid.instance.grid[(int)target.x, (int)target.z - 1].isOccupied)
            {
                nodes.Add(Grid.instance.grid[(int)target.x, (int)target.z - 1]);
            }
        }

        for (int i = 0; i < nodes.Count; i++)
        {
            //if (nodes[i].isEnemy && nodes[i].transform.position != defaultCase) { return defaultCase; }

            //if (nodes[i].isOccupied) { nodes.Remove(nodes[i]); continue; }

            nodes[i].enemySearchCost = Vector3.Distance(nodes[i].transform.position, defaultCase);
        }

        //print("lebngth" + nodes.Count);

        float min = 9999; int minIndex = -1;
        for (int i = 0; i < nodes.Count; i++)
        {
            //print(i + "    " + nodes[i].enemySearchCost);
            if (nodes[i].enemySearchCost < min)
            {
                //if(nodes[i].enemySearchCost == 0)
                //{
                //    return nodes[i].transform.position;
                //}
                min = nodes[i].enemySearchCost;
                minIndex = i;
            }
        }

        if (min != 9999)
        {
            //print("min" + minIndex);
            return nodes[minIndex].transform.position;
        }


        return defaultCase;
    }
}
