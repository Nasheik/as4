﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : Controller
{
    //[Header("Settings")]

    [Header("References")]
    public static CharacterController instance;
    public CharacterMotor selectedCharacterMotor;
    public CharacterMotor healCharacterMotor;
    public EnemyMotor selectedEnemyMotor;
    //public Queue<CharacterMotor> selectedCharacterMotors = new Queue<CharacterMotor>();
    //public Queue<EnemyMotor> selectedEnemyMotors = new Queue<EnemyMotor>();
    public GameObject characterOptionsCanvas;
    public GameObject mainOptionsCanvas;

    public GameObject healOption;
    public GameObject attackOption;

    public Image phaseBGImage;
    public Text phaseText;

    bool isChoosingPath;
    bool isChoosingAttack;
    bool isChoosingHeal;
    LineRenderer lineRenderer;
    public List<Node> viableMoveNodes = new List<Node>();
    public List<Node> viableAttackNodes = new List<Node>();

    public bool inCombat = false;
    public bool isDoneSelecting = false;
    //[Header("Prefabs")]

    public Color playerPhaseTextColor;
    public Color enemyPhaseTextColor;

    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);
    }

    void Start()
    {
        Cursor.instance.CharacterNodeSelected += SelectCharacter;
        Cursor.instance.EnemyNodeSelected += SelectEnemy;
        //characterOptionsCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(true);
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 0;
    }

    int frameOfSelection;
    int enemyFrameOfSelection;
    void Update()
    {
        if (inCombat && !selectedCharacterMotor)
        {
            phaseText.text = "Enemy's Turn";
            phaseText.color = enemyPhaseTextColor;
        }
        else
        {
            phaseText.text = "Your Turn";
            phaseText.color = playerPhaseTextColor;
        }

        if (selectedEnemyMotor == null)
        {

            enemyFrameOfSelection = Time.frameCount;
        }
        if (selectedCharacterMotor == null)
        {
            if (!inCombat)
            {
                characterOptionsCanvas.SetActive(false);
                mainOptionsCanvas.SetActive(true);
            }
            //MoveCursorMouse();
            //MoveCursorKeyboard();

            //if (Input.GetKeyDown(KeyCode.Return) || Input.GetMouseButtonDown(0))
            //{
            //    //SelectNode();
            //}
            frameOfSelection = Time.frameCount;


        }
        else if (isChoosingAttack)//Attack Option Selected
        {
            isChoosingPath = false;
            UndoShowMoveRadius();
            Cursor.instance.EnemyNodeSelected -= SelectEnemy;
            Cursor.instance.CharacterNodeSelected -= SelectCharacter;
            if (selectedCharacterMotor.remainingAttackCount == 0)
            {
                //PopupManager.instance.Popup("No Attacks Left");
                //print("No Attacks Left");
                selectedCharacterMotor = null;
                isChoosingAttack = false;
                Cursor.instance.EnemyNodeSelected += SelectEnemy;
                Cursor.instance.CharacterNodeSelected += SelectCharacter;
                //if (viableAttackNodes.Count != 0)
                //{
                //    UndoShowAttackRadius();
                //}
            }
            else
            {
                if (viableAttackNodes.Count == 0)
                {
                    ShowAttackRadius(selectedCharacterMotor.normalAttackDistance, true);//TODO Remove this from happening every frame
                    if (viableAttackNodes.Count == 0)
                    {
                        //PopupManager.instance.Popup("Nothing close\nenough to\nattack");
                        //print("Nothing close enough to attack");
                        //selectedCharacterMotor = null;
                        //isChoosingAttack = false;
                        //Cursor.instance.EnemyNodeSelected += SelectEnemy;
                        //Cursor.instance.CharacterNodeSelected += SelectCharacter;
                    }
                }
            }

            //ChooseCharacterAttackMouse();

            if (Input.GetMouseButtonDown(0) && !IsMouseInBlockInputZone())
            {
                if (Cursor.instance.selectedNode && viableAttackNodes.Contains(Cursor.instance.selectedNode) && Cursor.instance.selectedNode.isEnemy)
                {
                    selectedEnemyMotor = Cursor.instance.selectedNode.unit.GetComponent<EnemyMotor>();
                }
                Coroutine attackCoroutine = null;
                if (selectedEnemyMotor != null)
                {
                    attackCoroutine = StartCoroutine(CharacterAttack());
                }
                if (attackCoroutine == null)
                {
                    selectedCharacterMotor.outline.enabled = false;
                    selectedEnemyMotor = null;
                    selectedCharacterMotor = null;
                    Cursor.instance.EnemyNodeSelected += SelectEnemy;
                    Cursor.instance.CharacterNodeSelected += SelectCharacter;
                }
                isChoosingAttack = false;
                UndoShowAttackRadius();
                LevelManager.instance.CheckAndProcessWinState();
            }
        }
        else if (isChoosingHeal)//Heal Selected
        {
            isChoosingPath = false;
            UndoShowMoveRadius();
            Cursor.instance.CharacterNodeSelected -= SelectCharacter;
            Cursor.instance.EnemyNodeSelected -= SelectEnemy;
            //Cursor.instance.EnemyNodeSelected += SelectEnemy;
            if (selectedCharacterMotor.remainingAttackCount < 0)
            {
                PopupManager.instance.Popup("No Heals Left");
                print("No Heals Left");
                selectedCharacterMotor = null;
                isChoosingHeal = false;
                Cursor.instance.EnemyNodeSelected += SelectEnemy;
                Cursor.instance.CharacterNodeSelected += SelectCharacter;
                //if (viableAttackNodes.Count != 0)
                //{
                //    UndoShowAttackRadius();
                //}
            }
            else
            {
                if (viableHealNodes.Count == 0)
                {
                    ShowHealRadius(selectedCharacterMotor.normalAttackDistance);//TODO Remove this from happening every frame
                    if (viableHealNodes.Count == 0)
                    {
                        //PopupManager.instance.Popup("Nothing close\nenough to\nheal");
                        //print("Nothing close enough to heal");
                        //selectedCharacterMotor = null;
                        //isChoosingHeal = false;
                        //Cursor.instance.EnemyNodeSelected += SelectEnemy;
                        //Cursor.instance.CharacterNodeSelected += SelectCharacter;
                    }
                }
            }

            //ChooseCharacterAttackMouse();

            if (Input.GetMouseButtonDown(0) && !IsMouseInBlockInputZone())
            {
                if (Cursor.instance.selectedNode && viableHealNodes.Contains(Cursor.instance.selectedNode) && Cursor.instance.selectedNode.isCharacter)
                {
                    healCharacterMotor = Cursor.instance.selectedNode.unit.GetComponent<CharacterMotor>();
                    if (healCharacterMotor.remainingHealth >= healCharacterMotor.maxHealth)
                    {
                        PopupManager.instance.Popup("Already At Full Health");
                        healCharacterMotor = null;
                    }
                }
                else
                {
                    //popup no character there
                }
                Coroutine healCoroutine = null;
                if (healCharacterMotor != null)
                {
                    healCoroutine = StartCoroutine(CharacterHeal());
                }
                if (healCoroutine == null)
                {
                    selectedCharacterMotor.outline.enabled = false;
                    selectedCharacterMotor = null;
                    healCharacterMotor = null;
                    Cursor.instance.CharacterNodeSelected += SelectCharacter;
                    Cursor.instance.EnemyNodeSelected += SelectEnemy;
                }
                isChoosingHeal = false;
                UndoShowHealRadius();
                LevelManager.instance.CheckAndProcessWinState();
            }
        }
        else if (selectedEnemyMotor) { }
        else if (!inCombat)//Move Option Selected
        {
            if (selectedCharacterMotor.remainingAttackCount > 0)
            {
                Cursor.instance.selectedSkillButton.interactable = true;
                Cursor.instance.selectedSkillButton.image.color = Color.white;
            }
            else
            {
                Cursor.instance.selectedSkillButton.interactable = false;
            }

            Cursor.instance.CharacterNodeSelected -= SelectCharacter;
            Cursor.instance.EnemyNodeSelected -= SelectEnemy;
            if (selectedCharacterMotor)
            {
                if (selectedCharacterMotor.remainingStepCount == 0)
                {
                    //PopupManager.instance.Popup("No Steps Left");
                    //print("No Steps Left");
                    //selectedCharacterMotor = null;
                    //isChoosingPath = false;
                    //Cursor.instance.CharacterNodeSelected += SelectCharacter;
                    //return;
                }
                else
                {
                    if (viableMoveNodes.Count == 0)
                    {
                        ShowMoveRadius(true);//TODO Remove this from happening every frame
                    }
                }
            }

            ChooseCharacterPathMouse();
            //ChooseCharacterPathKeyboard();

            //print(Input.mousePosition + "     " + Screen.width + "     " + Screen.height + "     "  + Screen.currentResolution + "     "+ Screen.currentResolution.width / Input.mousePosition.x + "     " + Screen.currentResolution.height / Input.mousePosition.y);

            if (Time.frameCount > frameOfSelection + 1f && Input.GetMouseButtonDown(0) && !IsMouseInBlockInputZone())//Allows you to click the skill button
            {
                if (lineRenderer.positionCount > 0)
                {
                    MoveCharacter();
                    selectedCharacterMotor.remainingStepCount = 0;
                }
                else
                {
                    selectedCharacterMotor.outline.enabled = false;
                    selectedCharacterMotor = null;
                    isChoosingPath = false;
                    Cursor.instance.EnemyNodeSelected += SelectEnemy;
                    Cursor.instance.CharacterNodeSelected += SelectCharacter;
                }
                UndoShowMoveRadius();

                //selectedCharacterMotor = null;
                //isChoosingPath = false;
                //Cursor.instance.CharacterNodeSelected += SelectCharacter;
                //frameOfSelection = Time.frameCount;
            }
        }
        if (selectedCharacterMotor) //In Options Menu
        {
            if (!inCombat)
            {
                isChoosingPath = true;
                //characterOptionsCanvas.SetActive(true);
                mainOptionsCanvas.SetActive(false);
            }
            //print(selectedCharacterMotor);
            if (selectedCharacterMotor.characterType == CharacterType.Attacker)
            {
                attackOption.SetActive(true);
                healOption.SetActive(false);
                if (selectedCharacterMotor.remainingAttackCount <= 0)
                {
                    attackOption.GetComponent<Button>().interactable = false;
                }
                else
                {
                    attackOption.GetComponent<Button>().interactable = true;
                }
            }
            if (selectedCharacterMotor.characterType == CharacterType.Healer)
            {
                attackOption.SetActive(false);
                healOption.SetActive(true);
                if (selectedCharacterMotor.remainingAttackCount <= 0)
                {
                    healOption.GetComponent<Button>().interactable = false;
                }
                else
                {
                    healOption.GetComponent<Button>().interactable = true;
                }
            }
            Cursor.instance.EnemyNodeSelected -= SelectEnemy;
            Cursor.instance.CharacterNodeSelected -= SelectCharacter;
            //if (Input.GetMouseButtonDown(1))
            //{
            //    selectedCharacterMotor = null;
            //    UndoShowMoveRadius();
            //    Cursor.instance.CharacterNodeSelected += SelectCharacter;
            //}
        }
        else if (selectedEnemyMotor)
        {
            Cursor.instance.selectedSkillButton.image.color = Color.white;
            Cursor.instance.CharacterNodeSelected -= SelectCharacter;
            Cursor.instance.EnemyNodeSelected -= SelectEnemy;
            if (viableMoveNodes.Count == 0)
            {
                if (isEnemyChoosingAttack)
                {
                    Cursor.instance.selectedSkillButton.image.color = Color.grey;
                    ShowAttackRadius(selectedEnemyMotor.normalAttackDistance, false);
                }
                else
                {
                    ShowMoveRadius(false);//TODO Remove this from happening every frame 
                }
            }

            if (Time.frameCount > enemyFrameOfSelection + 1 && Input.GetMouseButtonDown(0) && !IsMouseInBlockInputZone())
            {
                selectedEnemyMotor.outline.enabled = false;
                selectedEnemyMotor = null;
                Cursor.instance.EnemyNodeSelected += SelectEnemy;
                Cursor.instance.CharacterNodeSelected += SelectCharacter;
                UndoShowMoveRadius();
                UndoShowAttackRadius();
                isEnemyChoosingAttack = false;
            }
        }
    }

    void SelectCharacter(CharacterMotor characterMotor)
    {
        selectedEnemyMotor = null;
        selectedCharacterMotor = characterMotor;
    }
    void SelectEnemy(EnemyMotor enemyMotor)
    {
        selectedCharacterMotor = null;
        selectedEnemyMotor = enemyMotor;
    }

    void MoveCharacter()
    {
        selectedCharacterMotor.MoveCharacter(lineRenderer);
    }

    void ShowMoveRadius(bool isCharacter)
    {
        UndoShowMoveRadius();
        if (isCharacter)
        {
            Grid.instance.GetViableMoveSpotsRadial(selectedCharacterMotor.transform.position, selectedCharacterMotor.remainingStepCount, viableMoveNodes);
        }
        else
        {
            Grid.instance.GetViableMoveSpotsRadial(selectedEnemyMotor.transform.position, selectedEnemyMotor.remainingStepCount, viableMoveNodes);
        }
    }

    void ShowAttackRadius(int radius, bool isCharacter)
    {
        //if (selectedMotor.isNormalRadial)
        //{
        //    GetViableMoveSpotsRadial(selectedMotor.transform.position, selectedMotor.normalAttackDistance);
        //}
        //else
        //{
        UndoShowAttackRadius();
        if (isCharacter)
        {
            Grid.instance.GetViableAttackSpotsLinear(selectedCharacterMotor.transform.position, radius, viableAttackNodes, true);
        }
        else
        {
            Grid.instance.GetViableAttackSpotsLinear(selectedEnemyMotor.transform.position, radius, viableAttackNodes, false);
        }
        //}
    }

    List<Node> viableHealNodes = new List<Node>();
    void ShowHealRadius(int radius)
    {
        UndoShowHealRadius();
        Grid.instance.GetViableHealSpotsLinear(selectedCharacterMotor.transform.position, radius, viableHealNodes, true);
    }

    void UndoShowHealRadius()
    {
        for (int i = 0; i < viableHealNodes.Count; i++)
        {
            Node selectedNode = viableHealNodes[i];
            //if (selectedNode.isOccupied && selectedNode.isCharacter)
            //{
            selectedNode.gameObject.GetComponent<MeshRenderer>().material.color = Grid.instance.notWalkableColor;
            selectedNode.isWalkable = false;
            selectedNode.searchIter = 0;
            //}
        }
        viableHealNodes.Clear();
    }

    void UndoShowMoveRadius()
    {
        for (int i = 0; i < viableMoveNodes.Count; i++)
        {
            Node selectedNode = viableMoveNodes[i];
            if (!selectedNode.isOccupied)
            {
                selectedNode.gameObject.GetComponent<MeshRenderer>().material.color = Grid.instance.notWalkableColor;
                selectedNode.isWalkable = false;
                selectedNode.searchIter = 0;
            }
        }
        viableMoveNodes.Clear();
    }

    void UndoShowAttackRadius()
    {
        //print("UndoShowAttackRadius" + viableAttackNodes.Count);
        for (int i = 0; i < viableAttackNodes.Count; i++)
        {
            Node selectedNode = viableAttackNodes[i];
            //if (!selectedNode.isOccupied)
            //{
            selectedNode.gameObject.GetComponent<MeshRenderer>().material.color = Grid.instance.notWalkableColor;
            selectedNode.isWalkable = false;
            selectedNode.searchIter = 0;
            //}
        }
        viableAttackNodes.Clear();
    }

    void ChooseCharacterPathMouse()
    {
        if (Grid.instance.grid[(int)Cursor.instance.cursor.transform.position.x, (int)Cursor.instance.cursor.transform.position.z].isWalkable)//TODO change this to be able to highlight over enemies?
        {
            Pathfinding.instance.CheckPathExistsAndSet(selectedCharacterMotor.transform.position, Cursor.instance.cursor.transform.position);
            Vector3[] path = Pathfinding.instance.mapGrid.GetTravelPath();
            for (int i = 0; i < path.Length; i++)
            {
                path[i] = new Vector3(path[i].x, path[i].y + .1f, path[i].z);
            }
            lineRenderer.positionCount = Mathf.Clamp(path.Length, 0, selectedCharacterMotor.remainingStepCount + 1);//Max step count line
            lineRenderer.SetPositions(path);
        }
        else
        {
            lineRenderer.positionCount = 0;
        }
    }

    #region OptionsCanvasCallbacks
    public void OnCallbackHealSelected()
    {
        if (inCombat && selectedCharacterMotor) return;
        isChoosingHeal = !isChoosingHeal;
        if (isChoosingHeal)
        {
            UndoShowMoveRadius();
            Cursor.instance.selectedSkillButton.image.color = Color.grey;
        }
        else
        {
            UndoShowHealRadius();
            Cursor.instance.selectedSkillButton.image.color = Color.white;
        }
        //characterOptionsCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(isChoosingHeal);
    }

    public void OnCallbackAttackSelected()
    {
        if (inCombat && selectedCharacterMotor) return;
        isChoosingAttack = !isChoosingAttack;
        if (isChoosingAttack)
        {
            UndoShowMoveRadius();
            Cursor.instance.selectedSkillButton.image.color = Color.grey;
        }
        else
        {
            UndoShowAttackRadius();
            Cursor.instance.selectedSkillButton.image.color = Color.white;
        }
        //characterOptionsCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(isChoosingAttack);
    }

    bool isEnemyChoosingAttack;
    public void OnCallbackEnemyAttakSelected()
    {
        if (inCombat) return;
        isEnemyChoosingAttack = !isEnemyChoosingAttack;
        if (isEnemyChoosingAttack)
        {
            UndoShowMoveRadius();
            //Cursor.instance.selectedSkillButton.image.color = Color.grey;
            //ShowAttackRadius(selectedEnemyMotor.normalAttackDistance, false);
        }
        else
        {
            UndoShowAttackRadius();
            //Cursor.instance.selectedSkillButton.image.color = Color.white;
            //ShowMoveRadius(false);
        }
    }

    public void OnCallbackMoveSelected()
    {
        isChoosingPath = true;
        //characterOptionsCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(true);
    }

    public void OnCallbackCancelSelected()
    {
        selectedCharacterMotor = null;
        Cursor.instance.CharacterNodeSelected += SelectCharacter;
        //characterOptionsCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(true);
    }
    #endregion

    IEnumerator CharacterHeal()
    {
        inCombat = true;
        if (healCharacterMotor != selectedCharacterMotor)
        {
            rotationReference.forward = healCharacterMotor.gameObject.transform.position - selectedCharacterMotor.transform.position;
            if (selectedCharacterMotor.transform.forward != rotationReference.forward)
            {
                yield return StartCoroutine(RotateObject(selectedCharacterMotor.transform, rotationReference, .5f));
            }
        }
        selectedCharacterMotor.GetComponentInChildren<Animator>().SetTrigger("Heal");
        yield return new WaitForSeconds(3.433f); //Hardcoded for Golem
        inCombat = false;
        //AudioManager.instance.Heal();
        healCharacterMotor.Heal(selectedCharacterMotor.normalAttackDamage);
        healCharacterMotor = null;
        //selectedCharacterMotor.remainingAttackCount--;
        selectedCharacterMotor.remainingAttackCount = 0;
        selectedCharacterMotor.remainingStepCount = 0;
    }

    IEnumerator CharacterAttack()
    {
        rotationReference.forward = selectedEnemyMotor.gameObject.transform.position - selectedCharacterMotor.transform.position;
        rotationReference.forward = rotationReference.right;

        //Cursor.instance.EnemyNodeSelected -= SelectEnemy;
        //selectedCharacterMotor.transform.rotation = Quaternion.LookRotation(selectedEnemyMotor.gameObject.transform.position - selectedCharacterMotor.transform.position);
        //selectedCharacterMotor.transform.forward = selectedCharacterMotor.transform.right;

        inCombat = true;

        if (selectedCharacterMotor.transform.forward != rotationReference.forward)
        {
            yield return StartCoroutine(RotateObject(selectedCharacterMotor.transform, rotationReference, .5f));
        }

        selectedCharacterMotor.GetComponentInChildren<Animator>().SetTrigger("PistolShot");


        //characterOptionsCanvas.SetActive(false);
        mainOptionsCanvas.SetActive(false);

        yield return new WaitForSeconds(.15f); //Hardcoded for PistolShot
        selectedCharacterMotor.EquipRevolver();
        yield return new WaitForSeconds(.91f); //Hardcoded for PistolShot
        selectedEnemyMotor.GetComponentInChildren<Animator>().SetTrigger("Hit");
        yield return new WaitForSeconds(.75f); //Hardcoded for HitBody
        rotationReference.forward = selectedEnemyMotor.gameObject.transform.position - selectedCharacterMotor.transform.position;
        selectedEnemyMotor.TakeDamage(selectedCharacterMotor.normalAttackDamage);
        yield return new WaitForSeconds(0.583f); //Hardcoded for HitBody
        selectedCharacterMotor.HolsterRevolver();
        yield return new WaitForSeconds(1.04f); //Hardcoded for PistolShot

        yield return StartCoroutine(RotateObject(selectedCharacterMotor.transform, rotationReference, .5f));

        selectedEnemyMotor = null;
        //selectedCharacterMotor.remainingAttackCount--;
        selectedCharacterMotor.remainingAttackCount = 0;
        selectedCharacterMotor.remainingStepCount = 0;
        selectedCharacterMotor = null;
        Cursor.instance.CharacterNodeSelected += SelectCharacter;
        //characterOptionsCanvas.SetActive(true);
        mainOptionsCanvas.SetActive(true);
        inCombat = false;
        //TODO Open Canvas
    }

    bool IsMouseInBlockInputZone()
    {
        if (Input.mousePosition.y > (Screen.height * .8f) && Input.mousePosition.x < (Screen.width * .25f))
        {
            return true;
        }
        return false;
    }

    /*
    void CharacterAttack()
    {
        //TODO Animation
        for (int i = 0; i < selectedEnemyMotors.Count; i++)
        {
            selectedEnemyMotors[i].TakeDamage(selectedCharacterMotor.normalAttackDamage);
            selectedCharacterMotor.remainingAttackCount--;
            if (selectedCharacterMotor.remainingAttackCount <= 0)
            {
                break;
            }
        }
    }

    void CharacterSpecialAttack()
    {
        //TODO Animation
        for (int i = 0; i < selectedEnemyMotors.Count; i++)
        {
            selectedEnemyMotors[i].TakeDamage(selectedCharacterMotor.specialAttackDamage);
        }
    }







    void ShowSpecialAttackRadius()
    {
        //if (selectedMotor.isSpecialRadial)
        //{
        //    GetViableMoveSpotsRadial(selectedMotor.transform.position, selectedMotor.specialAttackDistance);
        //}
        //else
        //{
        GetViableMoveSpotsLinear(selectedCharacterMotor.transform.position, selectedCharacterMotor.specialAttackDistance);
        //}
    }

    void ChooseCharacterSpecialAttackMouse()
    {

        //TODO Special Attack stuff
    }

    void ChooseCharacterAttackMouse()
    {
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100f, nodeLayer))
        {
            selectedEnemyMotors.Clear();
            if (viableAttackNodes.Contains(hit.collider.gameObject.GetComponent<Node>()))//TODO Affordances
            {
                cursor.transform.position = hit.collider.gameObject.transform.position;
                if (hit.collider.gameObject.GetComponent<Node>().unit.GetComponent<EnemyMotor>() != null)
                {
                    selectedEnemyMotors.Add(hit.collider.gameObject.GetComponent<Node>().unit.GetComponent<EnemyMotor>());
                }
            }
            else
            {
                cursor.transform.position = hit.collider.gameObject.transform.position;
                selectedEnemyMotors.Clear();
            }
        }
    }

*/
}