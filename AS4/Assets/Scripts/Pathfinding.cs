﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    //[Header("Settings")]

    [Header("References")]
    [HideInInspector] public Grid mapGrid;
    public Transform p1;
    public Transform p2;
    public static Pathfinding instance;

    //[Header("Prefabs")]

    void Awake()
    {
        if(instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        mapGrid = GetComponent<Grid>();
    }

    void Start()
    {
        //print(CheckPathExistsAndSet(p1.position, p2.position, true));
    }

    public bool CheckPathExistsAndSet(Vector3 startPos, Vector3 targetPos, bool setPath = true, bool forAI = false)
    {
        Node startNode = NodeFromWorldPoint(startPos);
        Node targetNode = NodeFromWorldPoint(targetPos);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node node = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost && openSet[i].hCost < node.hCost)
                {
                    if (openSet[i].hCost < node.hCost)
                    {
                        node = openSet[i];
                    }
                }
            }

            openSet.Remove(node);
            closedSet.Add(node);

            if (node == targetNode)
            {
                if (setPath)
                {
                    RetracePath(startNode, targetNode);
                }
                return true;
            }

            foreach (Node neighbour in mapGrid.GetNeighbours(node))
            {
                if (!forAI && (neighbour == null || !neighbour.isWalkable || closedSet.Contains(neighbour)))
                {
                    continue;
                }
                if (forAI && (neighbour == null ||
                    //(!neighbour.isWalkable && neighbour.isCharacter) ||
                    //(!neighbour.isWalkable && neighbour.isEnemy) ||
                    //(!neighbour.isWalkable && neighbour.isObstacle) ||
                    (!neighbour.isWalkable && neighbour.isOccupied) ||
                    closedSet.Contains(neighbour)))
                {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parent = node;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }
        return false;
    }

    void RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Add(startNode);
        path.Reverse();

        mapGrid.path = path;

    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        return (int)Vector3.Distance(nodeA.transform.position, nodeB.transform.position);

        //int dstX = Mathf.Abs((int)nodeA.gameObject.transform.position.x - (int)nodeB.gameObject.transform.position.x);
        //int dstY = Mathf.Abs((int)nodeA.gameObject.transform.position.y - (int)nodeB.gameObject.transform.position.y);
        //
        //if (dstX > dstY)
        //    return 14 * dstY + 10 * (dstX - dstY);
        //return 14 * dstX + 10 * (dstY - dstX);
    }



    Node NodeFromWorldPoint(Vector3 pos)
    {
        return mapGrid.grid[(int)pos.x, (int)pos.z];
    }

    Vector3 ConvertPointToTilePoint(Vector3 point)
    {
        float newX = ConvertFloatToTileFloat(point.x);
        float newZ = ConvertFloatToTileFloat(point.z);
        Vector3 newPoint = new Vector3(newX, point.y, newZ);
        return newPoint;
    }

    float ConvertFloatToTileFloat(float num)
    {
        if (num >= 0)
        {
            return (int)num;
        }
        else
        {
            return (int)num;
        }
    }
}