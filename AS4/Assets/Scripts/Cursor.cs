﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cursor : MonoBehaviour
{
    [Header("Settings")]
    public LayerMask nodeLayer;
    public Color enemyColor;
    public Color characterColor;
    public Color defaultColor;

    [Header("References")]
    public GameObject cursor;
    public Image cursorImage;

    RaycastHit selectHit;
    Camera mainCamera;
    public Node previousSelectedNode;
    public Node selectedNode;
    public static Cursor instance;

    public event Action<CharacterMotor> CharacterNodeSelected;
    public event Action<EnemyMotor> EnemyNodeSelected;

    cakeslice.Outline selectedOutline;
    cakeslice.Outline previousOutline;


    [Header("UIReferences")]
    public GameObject selectedUIParent;
    public Image selectedImage;
    public Text selectedName;
    public Image selectedAttackTypeImage;
    public Button selectedSkillButton;
    public Image selectedSkillImage;
    public Text selectedHpText;
    public Text selectedAttackText;
    public Text selectedMoveText;

    public GameObject targetUIParent;
    public Image targetImage;
    public Text targetName;
    public Image targetAttackTypeImage;
    public Text targetHpText;
    public Text targetAttackText;
    public Text targetMoveText;

    public Sprite attackUnitImage;
    public Sprite attackStatImage;
    public Sprite attackSkillImage;
    public Sprite healUnitImage;
    public Sprite healStatImage;
    public Sprite healSkillImage;



    void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(this);

        cursor = gameObject;
        //.transform.position = Vector3.one;
    }

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        MoveCursorMouse();
        if (Input.GetMouseButtonDown(0))
        {
            SelectNode();
        }
        //print((EnemyNodeSelected == null) + "      " + selectedNode.isEnemy);
    }

    void SelectNode()
    {
        //print(CharacterController.instance.inCombat);
        if (!CharacterController.instance.inCombat)
        {
            //print("in selectNode");
            if (CharacterNodeSelected != null && selectedNode != null && selectedNode.isCharacter)
            {
                CharacterNodeSelected(selectedNode.unit.GetComponent<CharacterMotor>());
            }
            else
            {
                //TODO Popup
                //print("Not Character");
            }
            if (EnemyNodeSelected != null && selectedNode != null && selectedNode.isEnemy)// && CharacterController.instance.viableAttackNodes.Contains(selectedNode))
            {
                //print("sleeccccc");
                EnemyNodeSelected(selectedNode.unit.GetComponent<EnemyMotor>());
            }
            else
            {
                //TODO Popup
                //print("Not Enemy or Not Reachable");
            }
            CharacterController.instance.isDoneSelecting = true;
        }
    }

    GameObject selectedGameObject;
    GameObject prevSelectedGameObject;

    void MoveCursorMouse()
    {
        if (CharacterController.instance.selectedCharacterMotor || CharacterController.instance.selectedEnemyMotor) // Display stats for selected unit
        {
            selectedUIParent.SetActive(true);
            Motor motor = CharacterController.instance.selectedCharacterMotor;
            if (motor == null) motor = CharacterController.instance.selectedEnemyMotor;
            selectedHpText.text = motor.remainingHealth.ToString();
            if (motor.characterType == CharacterType.Attacker)
            {
                selectedImage.sprite = attackUnitImage;
                selectedAttackTypeImage.sprite = attackStatImage;
                if (CharacterController.instance.selectedCharacterMotor)
                {
                    selectedSkillImage.sprite = attackSkillImage;
                    selectedSkillButton.onClick.RemoveAllListeners();
                    selectedSkillButton.onClick.AddListener(delegate { CharacterController.instance.OnCallbackAttackSelected(); });
                }
                else
                {
                    selectedSkillImage.sprite = attackSkillImage;
                    selectedSkillButton.onClick.RemoveAllListeners();
                    selectedSkillButton.onClick.AddListener(delegate { CharacterController.instance.OnCallbackEnemyAttakSelected(); });
                }
                selectedName.text = "Attacker";
            }
            else
            {
                selectedImage.sprite = healUnitImage;
                selectedAttackTypeImage.sprite = healStatImage;
                if (CharacterController.instance.selectedCharacterMotor)
                {
                    selectedSkillImage.sprite = healSkillImage;
                    selectedSkillButton.onClick.RemoveAllListeners();
                    selectedSkillButton.onClick.AddListener(delegate { CharacterController.instance.OnCallbackHealSelected(); });
                }
                else
                {
                    selectedSkillImage.sprite = healSkillImage;
                    selectedSkillButton.onClick.RemoveAllListeners();
                    //selectedSkillButton.onClick.AddListener(delegate { CharacterController.instance.OnCallbackHealSelected(); });
                }
                ////https://answers.unity.com/questions/854251/how-do-you-add-an-ui-eventtrigger-by-script.html
                ////selectedSkillButton.gameObject.GetComponent<EventTrigger>().
                selectedName.text = "Healer";
            }
            selectedAttackText.text = motor.normalAttackDamage.ToString();
            selectedMoveText.text = motor.maxStepCount.ToString();
            motor.outline.enabled = true;
        }
        else
        {
            selectedUIParent.SetActive(false);
        }

        if (selectedGameObject != null)
        {
            selectedGameObject.SetActive(false);

        }

        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100f, nodeLayer))
        {
            if (selectedNode && selectedNode.gameObject == hit.collider.gameObject)
            {
                //return;
            }
            else
            {
                if (selectedNode) previousSelectedNode = selectedNode;
                selectedNode = hit.collider.gameObject.GetComponent<Node>();
            }

            if (selectedNode)
            {
                selectedOutline = selectedNode.GetComponent<cakeslice.Outline>();
            }
            if (previousSelectedNode)
            {
                previousOutline = previousSelectedNode.GetComponent<cakeslice.Outline>();
            }
            if (selectedOutline)
            {
                selectedOutline.enabled = true;
                if (!selectedNode.isObstacle && selectedNode.unit)
                {
                    selectedNode.unit.GetComponent<Motor>().outline.enabled = true;
                }
                else if(selectedNode.unit)
                {
                    selectedNode.unit.GetComponent<cakeslice.Outline>().enabled = true;
                }
            }
            if (previousOutline)
            {
                previousOutline.enabled = false;
                if (!previousSelectedNode.isObstacle && previousSelectedNode.unit)
                {
                    previousSelectedNode.unit.GetComponent<Motor>().outline.enabled = false;
                }
                else if(previousSelectedNode.unit)
                {
                    previousSelectedNode.unit.GetComponent<cakeslice.Outline>().enabled = false;
                }
            }

            if (selectedNode.isOccupied && selectedNode.isCharacter)
            {
                targetUIParent.SetActive(true);
                //cursorImage.color = characterColor;
                selectedOutline.color = 1;
                //selectedGameObject = selectedNode.unit.transform.GetChild(1).gameObject;
                //selectedGameObject.SetActive(true);
                //selectedGameObject.transform.LookAt(-Camera.main.transform.position + 2 * selectedGameObject.transform.position, Camera.main.transform.rotation * Vector3.up);
                //selectedGameObject.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);


                CharacterMotor motor = selectedNode.unit.GetComponent<CharacterMotor>();
                targetHpText.text = motor.remainingHealth.ToString();
                if (motor.characterType == CharacterType.Attacker)
                {
                    targetImage.sprite = attackUnitImage;
                    targetAttackTypeImage.sprite = attackStatImage;
                    targetName.text = "Attacker";
                }
                else
                {
                    targetImage.sprite = healUnitImage;
                    targetAttackTypeImage.sprite = healStatImage;
                    targetName.text = "Healer";
                }
                targetAttackText.text = motor.normalAttackDamage.ToString();
                targetMoveText.text = motor.maxStepCount.ToString();


                //UIStats selectedUIStats = selectedGameObject.GetComponent<UIStats>();
                //selectedUIStats.hpText.text = selectedUIStats.motor.remainingHealth.ToString() + "/" + 25;
                //selectedUIStats.attackText.text = selectedUIStats.motor.normalAttackDamage.ToString();
                //selectedUIStats.stepText.text = selectedUIStats.motor.remainingStepCount.ToString() + "/" + 5;
            }
            else if (selectedNode.isOccupied && selectedNode.isEnemy)
            {
                targetUIParent.SetActive(true);
                //cursorImage.color = enemyColor;
                selectedOutline.color = 0;
                //selectedGameObject = selectedNode.unit.transform.GetChild(1).gameObject;
                //selectedGameObject.SetActive(true);
                //selectedGameObject.transform.LookAt(-Camera.main.transform.position + 2 * selectedGameObject.transform.position, Camera.main.transform.rotation * Vector3.up);
                ////selectedGameObject.transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position);

                //UIStats selectedUIStats = selectedGameObject.GetComponent<UIStats>();
                Motor motor = selectedNode.unit.GetComponent<Motor>();
                targetImage.sprite = attackUnitImage;
                targetName.text = "Attacker";
                targetHpText.text = motor.remainingHealth.ToString();
                targetAttackTypeImage.sprite = attackStatImage;
                targetAttackText.text = motor.normalAttackDamage.ToString();
                targetMoveText.text = motor.maxStepCount.ToString();
            }
            else if (selectedNode.isOccupied && selectedNode.isObstacle)
            {
                selectedOutline.color = 2;
                //TODO Display Obstacle Info
            }
            else
            {
                //cursorImage.color = defaultColor;
                selectedOutline.color = 3;
                targetUIParent.SetActive(false);
            }

            cursor.transform.position = hit.collider.gameObject.transform.position;
        }
        else
        {
            if (CharacterController.instance.selectedCharacterMotor)
            {
                cursor.transform.position = CharacterController.instance.selectedCharacterMotor.transform.position;
            }
            //else

            if (selectedOutline)
            {
                selectedOutline.enabled = false;
                if (!selectedNode.isObstacle && selectedNode.unit)
                {
                    selectedNode.unit.GetComponent<Motor>().outline.enabled = false;
                }
                else if (selectedNode.unit)
                {
                    selectedNode.unit.GetComponent<cakeslice.Outline>().enabled = false;
                }
            }
            if (previousOutline)
            {
                previousOutline.enabled = false;
                if (!previousSelectedNode.isObstacle && previousSelectedNode.unit)
                {
                    previousSelectedNode.unit.GetComponent<Motor>().outline.enabled = false;
                }
                else if (previousSelectedNode.unit)
                {
                    previousSelectedNode.unit.GetComponent<cakeslice.Outline>().enabled = false;
                }
            }
            if (selectedNode) selectedOutline = null;
            if (previousSelectedNode) previousOutline = null;

            selectedNode = null;
            previousSelectedNode = null;
        }

    }

    //Refractor this later
    //#region Keyboard Movement
    //void ChooseCharacterPathKeyboard()
    //{
    //    if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
    //    {
    //        Vector3 startingPos = cursor.transform.position;
    //        cursor.transform.position = ValidateMovementSpot(cursor.transform.position, Vector3.forward);
    //        lineRenderer.positionCount++;
    //        lineRenderer.SetPosition(lineRenderer.positionCount - 1, cursor.transform.position);
    //        RecorrectLineRenderer();
    //        if (lineRenderer.positionCount > selectedCharacterMotor.maxStepCount + 1)
    //        {
    //            lineRenderer.positionCount = selectedCharacterMotor.maxStepCount + 1;
    //            cursor.transform.position = startingPos;
    //        }
    //    }
    //    else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
    //    {
    //        Vector3 startingPos = cursor.transform.position;
    //        cursor.transform.position = ValidateMovementSpot(cursor.transform.position, Vector3.left);
    //        lineRenderer.positionCount++;
    //        lineRenderer.SetPosition(lineRenderer.positionCount - 1, cursor.transform.position);
    //        RecorrectLineRenderer();
    //        if (lineRenderer.positionCount > selectedCharacterMotor.maxStepCount + 1)
    //        {
    //            lineRenderer.positionCount = selectedCharacterMotor.maxStepCount + 1;
    //            cursor.transform.position = startingPos;
    //        }
    //    }
    //    else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
    //    {
    //        Vector3 startingPos = cursor.transform.position;
    //        cursor.transform.position = ValidateMovementSpot(cursor.transform.position, Vector3.back);
    //        lineRenderer.positionCount++;
    //        lineRenderer.SetPosition(lineRenderer.positionCount - 1, cursor.transform.position);
    //        RecorrectLineRenderer();
    //        if (lineRenderer.positionCount > selectedCharacterMotor.maxStepCount + 1)
    //        {
    //            lineRenderer.positionCount = selectedCharacterMotor.maxStepCount + 1;
    //            cursor.transform.position = startingPos;
    //        }
    //    }
    //    else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
    //    {
    //        Vector3 startingPos = cursor.transform.position;
    //        cursor.transform.position = ValidateMovementSpot(cursor.transform.position, Vector3.right);
    //        lineRenderer.positionCount++;
    //        lineRenderer.SetPosition(lineRenderer.positionCount - 1, cursor.transform.position);
    //        RecorrectLineRenderer();
    //        if (lineRenderer.positionCount > selectedCharacterMotor.maxStepCount + 1)
    //        {
    //            lineRenderer.positionCount = selectedCharacterMotor.maxStepCount + 1;
    //            cursor.transform.position = startingPos;
    //        }
    //    }
    //}
    //Vector3 ValidateCursorSpot(Vector3 currentPoint, Vector3 moveDirection)
    //{
    //    if (Physics.Raycast(transform.position + moveDirection + (Vector3.up * .05f), Vector3.down, out hit, 1f, nodeLayer))
    //    {
    //        Node selectedNode = hit.collider.gameObject.GetComponent<Node>();
    //        return currentPoint + moveDirection;
    //    }
    //    else
    //    {
    //        if (Physics.Raycast(transform.position + moveDirection + (Vector3.up * .05f), Vector3.up, out hit, 1f, nodeLayer))
    //        {
    //            Node selectedNode = hit.collider.gameObject.GetComponent<Node>();
    //            return currentPoint + moveDirection + Vector3.up;
    //        }
    //        else if (Physics.Raycast(transform.position + moveDirection + (Vector3.up * .05f), Vector3.down, out hit, 2f, nodeLayer))
    //        {
    //            Node selectedNode = hit.collider.gameObject.GetComponent<Node>();
    //            return currentPoint + moveDirection + Vector3.down;
    //        }
    //        else
    //        {
    //            return currentPoint;
    //        }
    //    }
    //}
    //void MoveCursorKeyboard()
    //{
    //    if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
    //    {
    //        cursor.transform.position = ValidateCursorSpot(cursor.transform.position, Vector3.forward);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
    //    {
    //        cursor.transform.position = ValidateCursorSpot(cursor.transform.position, Vector3.left);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
    //    {
    //        cursor.transform.position = ValidateCursorSpot(cursor.transform.position, Vector3.back);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
    //    {
    //        cursor.transform.position = ValidateCursorSpot(cursor.transform.position, Vector3.right);
    //    }
    //}

    //Vector3 ValidateMovementSpot(Vector3 currentPoint, Vector3 moveDirection)
    //{
    //    if (Physics.Raycast(transform.position + moveDirection + (Vector3.up * .05f), -transform.up, out hit, 1f, nodeLayer))
    //    {
    //        Node selectedNode = hit.collider.gameObject.GetComponent<Node>();
    //        if (selectedNode.isOccupied && selectedNode.isCharacter && selectedNode.unit == selectedCharacterMotor.gameObject || selectedNode.isWalkable)
    //        {
    //            return currentPoint + moveDirection;
    //        }
    //        //else if(!selectedNode.isWalkable && selectedNode.isObstacle)
    //        //{
    //        //    //TODO Obstacle Selected
    //        //    return currentPoint + moveDirection;
    //        //}
    //        //else if (!selectedNode.isWalkable && selectedNode.isEnemy)
    //        //{
    //        //    //TODO Enemy Selected
    //        //    return currentPoint + moveDirection;
    //        //}
    //        else
    //        {
    //            return currentPoint;
    //        }
    //    }
    //    else
    //    {
    //        if (Physics.Raycast(transform.position + moveDirection + (Vector3.up * .05f), Vector3.up, out hit, 1f, nodeLayer))
    //        {
    //            Node selectedNode = hit.collider.gameObject.GetComponent<Node>();
    //            if (selectedNode.isOccupied && selectedNode.isCharacter && selectedNode.unit == selectedCharacterMotor.gameObject || selectedNode.isWalkable)
    //            {
    //                return currentPoint + moveDirection + Vector3.up;
    //            }
    //            //else if (!selectedNode.isWalkable && selectedNode.isObstacle)
    //            //{
    //            //    //TODO Obstacle Selected
    //            //    return currentPoint + moveDirection + Vector3.up;
    //            //}
    //            //else if (!selectedNode.isWalkable && selectedNode.isEnemy)
    //            //{
    //            //    //TODO Enemy Selected
    //            //    return currentPoint + moveDirection + Vector3.up;
    //            //}
    //            else
    //            {
    //                return currentPoint;
    //            }
    //        }
    //        else if (Physics.Raycast(transform.position + moveDirection + (Vector3.up * .05f), Vector3.down, out hit, 2f, nodeLayer))
    //        {
    //            Node selectedNode = hit.collider.gameObject.GetComponent<Node>();
    //            if (selectedNode.isOccupied && selectedNode.isCharacter && selectedNode.unit == selectedCharacterMotor.gameObject || selectedNode.isWalkable)
    //            {
    //                return currentPoint + moveDirection + Vector3.down;
    //            }
    //            else if (!selectedNode.isWalkable && selectedNode.isObstacle)
    //            {
    //                //TODO Obstacle Selected
    //                return currentPoint + moveDirection + Vector3.down;
    //            }
    //            else if (!selectedNode.isWalkable && selectedNode.isEnemy)
    //            {
    //                //TODO Enemy Selected
    //                return currentPoint + moveDirection + Vector3.down;
    //            }
    //            else
    //            {
    //                return currentPoint;
    //            }
    //        }
    //        else
    //        {
    //            return currentPoint;
    //        }
    //    }
    //}

    //void RecorrectLineRenderer()
    //{
    //    Vector3[] linePoints = new Vector3[lineRenderer.positionCount];
    //    lineRenderer.GetPositions(linePoints);
    //    int index = System.Array.IndexOf(linePoints, cursor.transform.position);
    //    if (index > -1)
    //    {
    //        lineRenderer.positionCount = index + 1;
    //    }
    //}
    //#endregion


}