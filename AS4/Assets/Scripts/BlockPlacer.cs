﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BlockPlacer : MonoBehaviour
{
    [Header("Settings")]
    public LayerMask groundLayer;

    [Header("References")]
    public GameObject blockPrefab;
    Camera mainCamera;

    //[Header("Prefabs")]

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            PlaceBlock();
        }
    }

    void PlaceBlock()
    {
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        print(mainCamera.transform.position + "    " + ray.direction);
        Debug.DrawRay(mainCamera.transform.position, ray.direction, Color.blue);
        if (Physics.Raycast(ray, out hit, 100f, groundLayer))
        {
            Instantiate(blockPrefab, hit.point, Quaternion.FromToRotation(transform.up, hit.normal));
        }
    }
}
