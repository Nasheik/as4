﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    [Header("Settings")]
    public LayerMask obstacleLayer;

    [Header("References")]
    public bool isWalkable;
    public bool isOccupied;
    public bool isEnemy;
    public bool isCharacter;
    public bool isObstacle;

    public int gCost;
    public int hCost;
    public int searchIter;
    public float enemySearchCost;
    public Node parent;

    public GameObject unit;

    public cakeslice.Outline outline;

    //[Header("Prefabs")]

    void Awake()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position+Vector3.down,Vector3.up,out hit, 200f, obstacleLayer))
        {
            isWalkable = false;
            isOccupied = true;
            isObstacle = true;
            unit = hit.collider.gameObject;
            GetComponent<MeshRenderer>().material.color = Grid.instance.obstacleColor;
            unit.GetComponent<cakeslice.Outline>().enabled = false;
        }
        outline.enabled = false;
    }

    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }
}