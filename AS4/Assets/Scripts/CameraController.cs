﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Camera Options")]
    public bool useCameraMovement;
    public bool useCameraYaw;
    public bool useCameraPitch;
    public bool useCameraRoll;
    public bool useCameraZoom;

    [Header("Camera Settings")]
    public float cameraMovementSpeed = .2f;
    public float zoomSensitivity = 10f;
    public float minFieldOfView = 15f;
    public float maxFieldOfView = 90f;
    public float cameraRotationSpeed = .5f;

    [Header("References")]
    public Camera mainCamera;

    [Header("Private")]
    Vector2 currentMousePos;
    Vector2 prevMousePos;

    void Update()
    {
        prevMousePos = currentMousePos;
        currentMousePos = Input.mousePosition;

        if (useCameraMovement) Translation();
        if (useCameraYaw) Yaw();
        if (useCameraPitch) Pitch();
        if (useCameraRoll) Roll();
        if (useCameraZoom) Zoom();
    }


    void Translation()
    {
        Vector3 cameraForward = mainCamera.transform.forward;
        Vector3 inputVert = new Vector3(Input.GetAxisRaw("Vertical") * cameraForward.x * cameraMovementSpeed, 0, Input.GetAxisRaw("Vertical") * cameraForward.z * cameraMovementSpeed);
        Vector3 inputHorz = new Vector3(Input.GetAxisRaw("Horizontal") * cameraForward.z * cameraMovementSpeed, 0, -Input.GetAxisRaw("Horizontal") * cameraForward.x * cameraMovementSpeed);

        transform.localPosition += (inputVert + inputHorz);
    }

    void Yaw()
    {
        if (Input.GetMouseButton(1))
        {
            transform.eulerAngles += Vector3.up * (currentMousePos.x - prevMousePos.x) * cameraRotationSpeed;
        }
    }

    void Pitch()
    {
        if (Input.GetMouseButton(1))
        {
            transform.eulerAngles += Vector3.right * (currentMousePos.y - prevMousePos.y) * cameraRotationSpeed/2;
        }
    }

    void Roll()
    {
        if (Input.GetMouseButton(0))
        {
            transform.eulerAngles += Vector3.forward * (currentMousePos.x - prevMousePos.x) * cameraRotationSpeed;
        }
    }

    void Zoom()
    {
        float fieldOfView = mainCamera.fieldOfView;
        fieldOfView -= Input.GetAxisRaw("Mouse ScrollWheel") * zoomSensitivity;
        fieldOfView = Mathf.Clamp(fieldOfView, minFieldOfView, maxFieldOfView);
        mainCamera.fieldOfView = fieldOfView;
    }
}