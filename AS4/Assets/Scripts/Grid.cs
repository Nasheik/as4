﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class Grid : MonoBehaviour
{
    [Header("Settings")]
    public int depth;
    public int width;
    public LayerMask groundLayer;
    public Node[,] grid;
    public Color walkableColor;
    public Color notWalkableColor;
    public Color attackableColor;
    public Color healableColor;
    public Color obstacleColor;

    [Header("References")]
    Vector3 startPosition;
    public List<Node> path;
    public static Grid instance;

    [Header("Prefabs")]
    public GameObject nodePrefab;

    //void OnDrawGizmos()
    //{
    //    if (grid != null)
    //    {
    //        foreach (Node n in grid)
    //        {
    //            if (n == null) continue;
    //            Gizmos.color = (n.isWalkable) ? Color.white : Color.red;
    //            if (path.Count > 0)
    //            {
    //                if (path.Contains(n))
    //                {
    //                    Gizmos.color = Color.black;
    //                }
    //            }
    //            Gizmos.DrawCube(n.gameObject.transform.position, Vector3.one);
    //        }
    //    }
    //}

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        grid = new Node[width, depth];
        startPosition = transform.position;
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < depth; z++)
            {
                RaycastHit hit;
                Vector3 currPos = new Vector3(transform.position.x + x, transform.position.y + 100, transform.position.z + z);
                if (Physics.Raycast(currPos, Vector3.down, out hit, 200f, groundLayer))
                {
                    grid[x, z] = Instantiate(nodePrefab, new Vector3(transform.position.x + x, hit.point.y, transform.position.z + z), Quaternion.FromToRotation(Vector3.up, hit.normal), transform).gameObject.GetComponent<Node>();
                }
            }
        }
    }

    public Vector3[] GetTravelPath()
    {
        List<Vector3> pathPoints = new List<Vector3>();
        foreach (Node node in path)
        {
            pathPoints.Add(node.gameObject.transform.position);
        }
        return pathPoints.ToArray();
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (Mathf.Abs(x) == Mathf.Abs(y))
                    continue;

                int checkX = (int)node.gameObject.transform.position.x + x;
                int checkY = (int)node.gameObject.transform.position.z + y;

                if (checkX >= 0 && checkX < width && checkY >= 0 && checkY < depth)
                {
                    //print(x + "  " + y + "  added");
                    neighbours.Add(grid[checkX, checkY]);
                }
                else
                {
                    //print(x + "  " + y + "  NOT ADDED");
                }
            }
        }
        return neighbours;
    }

    public void GetViableHealSpotsLinear(Vector3 pos, int numIterLeft, List<Node> viableHealNodes, bool isCharacter)
    {

        bool checkForEnemy = isCharacter;
        bool checkForCharacter = !isCharacter;

        int x = (int)pos.x; int z = (int)pos.z;

        viableHealNodes.Add(grid[x, z]);
        grid[x, z].gameObject.GetComponent<MeshRenderer>().material.color = healableColor;

        int minX = Mathf.Clamp(x - numIterLeft, 0, Pathfinding.instance.mapGrid.width - 1);
        int maxX = Mathf.Clamp(x + numIterLeft, 0, Pathfinding.instance.mapGrid.width - 1);
        int minZ = Mathf.Clamp(z - numIterLeft, 0, Pathfinding.instance.mapGrid.depth - 1);
        int maxZ = Mathf.Clamp(z + numIterLeft, 0, Pathfinding.instance.mapGrid.depth - 1);

        for (int i = x; i >= minX; i--)
        {
            if (i == x) continue;

            if (grid[i, z] != null)// && grid[i, z].isCharacter)
            {
                if (grid[i, z].isObstacle || (grid[i, z].isEnemy && checkForEnemy) || (grid[i, z].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[i, z].gameObject.GetComponent<MeshRenderer>().material.color = healableColor;
                if (!viableHealNodes.Contains(grid[i, z]))
                {
                    viableHealNodes.Add(grid[i, z]);
                }
                if ((grid[i, z].isEnemy && !checkForEnemy) || (grid[i, z].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }
        for (int i = x; i <= maxX; i++)
        {
            if (i == x) continue;

            if (grid[i, z] != null)// && grid[i, z].isCharacter)
            {
                if (grid[i, z].isObstacle || (grid[i, z].isEnemy && checkForEnemy) || (grid[i, z].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[i, z].gameObject.GetComponent<MeshRenderer>().material.color = healableColor;
                if (!viableHealNodes.Contains(grid[i, z]))
                {
                    viableHealNodes.Add(grid[i, z]);
                }
                if ((grid[i, z].isEnemy && !checkForEnemy) || (grid[i, z].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }

        for (int i = z; i >= minZ; i--)
        {
            if (i == z) continue;

            if (grid[x, i] != null)// && grid[x, i].isCharacter)
            {
                if (grid[x, i].isObstacle || (grid[x, i].isEnemy && checkForEnemy) || (grid[x, i].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[x, i].gameObject.GetComponent<MeshRenderer>().material.color = healableColor;
                if (!viableHealNodes.Contains(grid[x, i]))
                {
                    viableHealNodes.Add(grid[x, i]);
                }
                if ((grid[x, i].isEnemy && !checkForEnemy) || (grid[x, i].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }
        for (int i = z; i <= maxZ; i++)
        {
            if (i == z) continue;

            if (grid[x, i] != null)// && grid[x, i].isCharacter)
            {
                if (grid[x, i].isObstacle || (grid[x, i].isEnemy && checkForEnemy) || (grid[x, i].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[x, i].gameObject.GetComponent<MeshRenderer>().material.color = healableColor;
                if (!viableHealNodes.Contains(grid[x, i]))
                {
                    viableHealNodes.Add(grid[x, i]);
                }
                if ((grid[x, i].isEnemy && !checkForEnemy) || (grid[x, i].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }
    }


    #region GetViableAttackSpots
    public void GetViableAttackSpotsLinear(Vector3 pos, int numIterLeft, List<Node> viableAttackNodes, bool isCharacter)
    {
        bool checkForEnemy = !isCharacter;
        bool checkForCharacter = isCharacter;

        int x = (int)pos.x; int z = (int)pos.z;

        int minX = Mathf.Clamp(x - numIterLeft, 0, Pathfinding.instance.mapGrid.width - 1);
        int maxX = Mathf.Clamp(x + numIterLeft, 0, Pathfinding.instance.mapGrid.width - 1);
        int minZ = Mathf.Clamp(z - numIterLeft, 0, Pathfinding.instance.mapGrid.depth - 1);
        int maxZ = Mathf.Clamp(z + numIterLeft, 0, Pathfinding.instance.mapGrid.depth - 1);

        for (int i = x; i >= minX; i--)
        {
            if (i == x) continue;
            if (grid[i, z] != null)// && grid[i, z].isEnemy)
            {
                if (grid[i, z].isObstacle || (grid[i, z].isEnemy && checkForEnemy) || (grid[i, z].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[i, z].gameObject.GetComponent<MeshRenderer>().material.color = attackableColor;
                if (!viableAttackNodes.Contains(grid[i, z]))
                {
                    viableAttackNodes.Add(grid[i, z]);
                }
                if ((grid[i, z].isEnemy && !checkForEnemy) || (grid[i, z].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }
        for (int i = x; i <= maxX; i++)
        {
            if (i == x) continue;
            if (grid[i, z] != null)// && grid[i, z].isEnemy)
            {
                if (grid[i, z].isObstacle || (grid[i, z].isEnemy && checkForEnemy) || (grid[i, z].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[i, z].gameObject.GetComponent<MeshRenderer>().material.color = attackableColor;
                if (!viableAttackNodes.Contains(grid[i, z]))
                {
                    viableAttackNodes.Add(grid[i, z]);
                }
                if ((grid[i, z].isEnemy && !checkForEnemy) || (grid[i, z].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }

        for (int i = z; i >= minZ; i--)
        {
            if (i == z) continue;
            if (grid[x, i] != null)// && grid[x, i].isEnemy)
            {
                if (grid[x, i].isObstacle || (grid[x, i].isEnemy && checkForEnemy) || (grid[x, i].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[x, i].gameObject.GetComponent<MeshRenderer>().material.color = attackableColor;
                if (!viableAttackNodes.Contains(grid[x, i]))
                {
                    viableAttackNodes.Add(grid[x, i]);
                }
                if ((grid[x, i].isEnemy && !checkForEnemy) || (grid[x, i].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }
        for (int i = z; i <= maxZ; i++)
        {
            if (i == z) continue;
            if (grid[x, i] != null)// && grid[x, i].isEnemy)
            {
                if (grid[x, i].isObstacle || (grid[x, i].isEnemy && checkForEnemy) || (grid[x, i].isCharacter && checkForCharacter))
                {
                    break;
                }
                grid[x, i].gameObject.GetComponent<MeshRenderer>().material.color = attackableColor;
                if (!viableAttackNodes.Contains(grid[x, i]))
                {
                    viableAttackNodes.Add(grid[x, i]);
                }
                if ((grid[x, i].isEnemy && !checkForEnemy) || (grid[x, i].isCharacter && !checkForCharacter))
                {
                    break;
                }
            }
        }
    }
    #endregion

    #region GetViableMoveSpots
    public void GetViableMoveSpotsLinear(Vector3 pos, int numIterLeft, List<Node> viableMoveNodes)
    {
        int x = (int)pos.x; int z = (int)pos.z;

        int minX = Mathf.Clamp(x - numIterLeft, 0, Pathfinding.instance.mapGrid.width - 1);
        int maxX = Mathf.Clamp(x + numIterLeft, 0, Pathfinding.instance.mapGrid.width - 1);
        int minZ = Mathf.Clamp(z - numIterLeft, 0, Pathfinding.instance.mapGrid.depth - 1);
        int maxZ = Mathf.Clamp(z + numIterLeft, 0, Pathfinding.instance.mapGrid.depth - 1);

        for (int i = x; i >= minX; i--)
        {
            if (i == x) continue;
            if (grid[i, z] != null && !grid[i, z].isOccupied)
            {
                grid[i, z].isWalkable = true;
                grid[i, z].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
                if (!viableMoveNodes.Contains(grid[i, z]))
                {
                    viableMoveNodes.Add(grid[i, z]);
                }
            }
            else
            {
                break;
            }
        }
        for (int i = x; i <= maxX; i++)
        {
            if (i == x) continue;
            if (grid[i, z] != null && !grid[i, z].isOccupied)
            {
                grid[i, z].isWalkable = true;
                grid[i, z].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
                if (!viableMoveNodes.Contains(grid[i, z]))
                {
                    viableMoveNodes.Add(grid[i, z]);
                }
            }
            else
            {
                break;
            }
        }

        for (int i = z; i >= minZ; i--)
        {
            if (i == z) continue;
            if (grid[x, i] != null && !grid[x, i].isOccupied)
            {
                grid[x, i].isWalkable = true;
                grid[x, i].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
                if (!viableMoveNodes.Contains(grid[x, i]))
                {
                    viableMoveNodes.Add(grid[x, i]);
                }
            }
            else
            {
                break;
            }
        }
        for (int i = z; i <= maxZ; i++)
        {
            if (i == z) continue;
            if (grid[x, i] != null && !grid[x, i].isOccupied)
            {
                grid[x, i].isWalkable = true;
                grid[x, i].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
                if (!viableMoveNodes.Contains(grid[x, i]))
                {
                    viableMoveNodes.Add(grid[x, i]);
                }
            }
            else
            {
                break;
            }
        }

        return;
    }

    public void GetViableMoveSpotsRadial(Vector3 pos, int numIterLeft, List<Node> viableMoveNodes)
    {
        //print(dir);

        if (numIterLeft == 0) return;

        int x = (int)pos.x; int z = (int)pos.z;

        //if (x <= 0 || z <= 0) return;

        if (z + 1 < Pathfinding.instance.mapGrid.depth && grid[x, z + 1].searchIter < numIterLeft && grid[x, z + 1] != null && !grid[x, z + 1].isOccupied)//North
        {
            //print(dir);
            grid[x, z + 1].hCost = numIterLeft;
            grid[x, z + 1].isWalkable = true;
            grid[x, z + 1].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
            if (!viableMoveNodes.Contains(grid[x, z + 1]))
            {
                viableMoveNodes.Add(grid[x, z + 1]);
            }
            GetViableMoveSpotsRadial(new Vector3(x, 0, z + 1), numIterLeft - 1, viableMoveNodes);
        }

        if (z - 1 >= 0 && grid[x, z - 1].searchIter < numIterLeft && grid[x, z - 1] != null && !grid[x, z - 1].isOccupied)//South
        {
            grid[x, z - 1].searchIter = numIterLeft;
            grid[x, z - 1].isWalkable = true;
            grid[x, z - 1].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
            if (!viableMoveNodes.Contains(grid[x, z - 1]))
            {
                viableMoveNodes.Add(grid[x, z - 1]);
            }
            GetViableMoveSpotsRadial(new Vector3(x, 0, z - 1), numIterLeft - 1, viableMoveNodes);
        }

        if (x - 1 >= 0 && grid[x - 1, z].searchIter < numIterLeft && grid[x - 1, z] != null && !grid[x - 1, z].isOccupied)//West
        {
            grid[x - 1, z].searchIter = numIterLeft;
            grid[x - 1, z].isWalkable = true;
            grid[x - 1, z].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
            if (!viableMoveNodes.Contains(grid[x - 1, z]))
            {
                viableMoveNodes.Add(grid[x - 1, z]);
            }
            GetViableMoveSpotsRadial(new Vector3(x - 1, 0, z), numIterLeft - 1, viableMoveNodes);
        }

        if (x + 1 < Pathfinding.instance.mapGrid.width && grid[x + 1, z].searchIter < numIterLeft && grid[x + 1, z] != null && !grid[x + 1, z].isOccupied)//East
        {
            grid[x + 1, z].searchIter = numIterLeft;
            grid[x + 1, z].isWalkable = true;
            grid[x + 1, z].gameObject.GetComponent<MeshRenderer>().material.color = walkableColor;
            if (!viableMoveNodes.Contains(grid[x + 1, z]))
            {
                viableMoveNodes.Add(grid[x + 1, z]);
            }
            GetViableMoveSpotsRadial(new Vector3(x + 1, 0, z), numIterLeft - 1, viableMoveNodes);
        }

        return;
    }
    #endregion
}