﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    //[Header("Settings")]

    //[Header("References")]
    public Transform rotationReference;

    //[Header("Prefabs")]

    void Awake()
    {

    }

    void Start()
    {

    }

    void Update()
    {

    }

    protected IEnumerator RotateObject(Transform transformToRotate, Transform transformToRotateTo, float seconds)
    {
        float startTime = Time.time;
        float endTime = Time.time + seconds;

        Quaternion startRotation = transformToRotate.rotation;
        Quaternion endRotation = transformToRotateTo.rotation;

        while (Time.time < endTime)
        {
            transformToRotate.rotation = Quaternion.Lerp(startRotation, endRotation, (Time.time - startTime) / seconds);
            yield return null;
        }

        transformToRotate.rotation = endRotation;
    }
}
