﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class CharacterMotor : Motor
{
    void Start()
    {
        remainingAttackCount = maxAttackCount;
        remainingStepCount = maxStepCount;
        remainingHealth = maxHealth;
        SetCurrentPositionNode();
        LevelManager.instance.OnNewRound += NewRound;
        LevelManager.instance.allCharacters.Add(this);
        outline.enabled = false;
    }

    public void MoveCharacter(LineRenderer path)
    {
        remainingStepCount = remainingStepCount - (path.positionCount-1);
        Vector3[] points = new Vector3[path.positionCount];
        path.GetPositions(points);
        if (movementCoroutine != null)
        {
            StopCoroutine(movementCoroutine);
        }
        movementCoroutine = StartCoroutine(ProcessMovement(points));
        path.positionCount = 0;
    }

    public override void SetCurrentPositionNode()
    {
        RaycastHit hit;
        //print("set current position");
        if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 1.25f, nodeLayer))
        {

            Node currentPositionNode = hit.collider.gameObject.GetComponent<Node>();
            currentPositionNode.isOccupied = true;
            currentPositionNode.isCharacter = true;
            currentPositionNode.unit = gameObject;
        }
    }

    public override void RemoveCurrentPositionNode()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up, -transform.up, out hit, 2f, nodeLayer))
        {
            Node currentPositionNode = hit.collider.gameObject.GetComponent<Node>();
            currentPositionNode.isOccupied = false;
            currentPositionNode.isCharacter = false;
            currentPositionNode.unit = null;
        }
    }

    void OnDestroy()
    {
        LevelManager.instance.allCharacters.Remove(this);    
    }

    public override void NewRound()
    {
        LevelManager.instance.CheckAndProcessFailState();
        if(!CharacterController.instance.mainOptionsCanvas.activeSelf) CharacterController.instance.mainOptionsCanvas.SetActive(true);
        base.NewRound();
    }
}